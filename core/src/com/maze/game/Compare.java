package com.maze.game;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by zachm on 10/19/2017.
 */
public class Compare {
	public static boolean Vector(Vector2 a, Vector2 b, float delta) {
		float d = new Vector2(a).sub(b).len();
		return (d < delta);
	}

	public static boolean Vector(Vector2 a, Vector2 b, float deltaX, float deltaY) {
		Vector2 d = new Vector2(a).sub(b);
		return (Math.abs(d.x) <= deltaX && Math.abs(d.y) <= deltaY);
	}

	public static boolean Float(float a, float b, float delta) {
		return (a + delta > b && a - delta < b);
	}
}
