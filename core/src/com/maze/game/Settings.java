package com.maze.game;

import com.badlogic.gdx.Input;

public class Settings {
    public static int up = Input.Keys.W;
    public static int down = Input.Keys.S;
    public static int left = Input.Keys.A;
    public static int right = Input.Keys.D;
    public static int sprint = Input.Keys.SHIFT_LEFT;
    public static int invisible = Input.Keys.SPACE;

    public static int pause = Input.Keys.ESCAPE;
    public static int map = Input.Keys.ALT_LEFT;

    public static int profile = Input.Keys.P;
    public static int godMode = Input.Keys.G;
    public static int cheat1 = Input.Keys.L;
    public static int cheat2 = Input.Keys.K;
}
