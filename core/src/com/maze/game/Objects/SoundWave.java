package com.maze.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.AnimatedEntity;
import com.maze.game.BaseObjects.DrawLight;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Stages.MazeStage;
import com.maze.game.Time;

/**
 * Created by zachm on 10/10/2017.
 */
public class SoundWave extends AnimatedEntity implements DrawLight {
	MazeStage stage;
	public SoundWave(Vector2 pos) {
		super(pos, 0, 100, null);
		setAnim(ResourceManager.getAnim("SoundWave/SoundWave.png", 50, 1/60f), 0);
		anim.setPlayMode(Animation.PlayMode.NORMAL);
		looping = false;
		color = Color.SKY;

		if (Game.currentStage instanceof MazeStage) {
			stage = (MazeStage) Game.currentStage;
		}
	}

	@Override
	public void update() {
		if (anim.isAnimationFinished(stateTime)) {
			Delete();
		}
	}

	@Override
	public void render(Batch batch) {

	}

	@Override
	public void renderLight(Batch graphics) {
		if (stage.player != null && stage.player.invisible) {
			super.render(graphics);
		} else {
			stateTime += Time.deltaTime;
		}
	}

	@Override
	public int lightz() {
		return z;
	}
}
