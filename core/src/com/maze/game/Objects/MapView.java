package com.maze.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.DrawableGUI;
import com.maze.game.BaseObjects.GUIBox;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Stages.MazeStage;

/**
 * Created by zachm on 10/17/2017.
 */
public class MapView extends GameObject implements DrawableGUI {
	MazeStage stage;
	GUIBox window;
	Texture floor;
	Texture wall;
	Texture player;
	Texture gate;
	Texture exit;

	StringDraw explore;
	GUIBox exploreBox;

	int tileCount = 0;
	int exploredTiles = 0;

	public boolean visible;
	public boolean[][] explored;
	public boolean fullyExplored;

	public MapView() {
		super(0, 0, 0, 20);
		window = new GUIBox(0, 0, 300, 300, 10);
		floor = ResourceManager.getImage("GUI/Map/mazeFloor.png");
		wall = ResourceManager.getImage("GUI/Map/mazeWall.png");
		player = ResourceManager.getImage("GUI/Map/player.png");
		gate = ResourceManager.getImage("GUI/Map/gate.png");
		exit = ResourceManager.getImage("GUI/Map/exit.png");

		explore = new StringDraw("Fully Explored", 0, -200, Color.GOLD, Game.fonts[2], 40, 4);
		exploreBox = new GUIBox(0, -200, 250, 50, 30);
	}

	@Override
	public void update() {
		super.update();
		stage = (MazeStage) Game.currentStage;

		if (explored == null) {
			explored = new boolean[stage.maze.width * 2 + 1][stage.maze.width * 2 + 1];
		}

		tileCount = (stage.maze.width * 2 + 1) * (stage.maze.height * 2 + 1);

		if (stage.player != null && !stage.player.delete) {
			Vector2 tilePos = stage.maze.posToTile(stage.player.pos.x, stage.player.pos.y);
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (tilePos.x - x >= 0 && tilePos.x -x < explored.length) {
						if (tilePos.y - y >= 0 && tilePos.y - y < explored[0].length) {
							if (!explored[(int) tilePos.x - x][(int) tilePos.y - y]) {
								explored[(int) tilePos.x - x][(int) tilePos.y - y] = true;
								exploredTiles += 1;
							}
						}
					}
				}
			}
		}

		fullyExplored = (exploredTiles == tileCount);

		explore.update();
	}

	@Override
	public void renderGUI(Batch batch) {
		if (stage.player != null && visible && !stage.player.invisible) {
			window.renderGUI(batch);
			float mapScale = 300f / ((stage.maze.width * 2 + 1) * 3);
			float xOff = -150;
			float yOff = -150;

			for (int x = 0; x < stage.maze.width * 2 + 1; x++) {
				for (int y = 0; y < stage.maze.height * 2 + 1; y++) {
					if (explored[x][y] | Game.debug) {
						if (x == stage.maze.width * 2 | y == stage.maze.height * 2) {
							batch.draw(wall, xOff + mapScale * 3 * x, yOff + mapScale * 3 * y, 3 * mapScale, 3 * mapScale);
						} else {
							if (stage.maze.tileOpen[x][y]) {
								batch.draw(floor, xOff + mapScale * 3 * x, yOff + mapScale * 3 * y, 3 * mapScale, 3 * mapScale);
							} else {
								batch.draw(wall, xOff + mapScale * 3 * x, yOff + mapScale * 3 * y, 3 * mapScale, 3 * mapScale);
							}

						}
					}
				}
			}

			Vector2 tilePos = stage.maze.posToTile(stage.player.pos.x, stage.player.pos.y);
			batch.draw(player, xOff + tilePos.x * 3 * mapScale, yOff + tilePos.y * 3 * mapScale, 3 * mapScale, 3 * mapScale);

			if (exploredTiles == tileCount) {
				exploreBox.renderGUI(batch);
				explore.renderGUI(batch);
			}

			if (Game.debug) {
				for (int i = 0; i < stage.maze.endpoints.size(); i++) {
					tilePos = stage.maze.posToTile(stage.maze.endpoints.get(i).x, stage.maze.endpoints.get(i).y);
					batch.draw(player, xOff + tilePos.x * 3 * mapScale, yOff + tilePos.y * 3 * mapScale, 3 * mapScale, 3 * mapScale);
				}
			}
		}

	}

	@Override
	public int guiz() {
		return z;
	}
}
