package com.maze.game.Objects;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.maze.game.BaseObjects.GUIBox;
import com.maze.game.BaseObjects.GUITile;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.Game;
import com.maze.game.Settings;
import com.maze.game.Stages.StartMenu;
import com.maze.game.Time;

import java.util.ArrayList;

/**
 * Created by zachm on 10/26/2017.
 */
public class ControllsWindow extends GameObject {
	public ControllsWindow() {
		super(0, 0, 0, 10);
		int menux = 175;
		int menuy = 170;
		int offset = -25;

		Game.addGameObject(new GUIBox(menux, menuy - 170, 200, 400, z)).setParent(this);

		StringDraw s = (StringDraw) Game.addGameObject(new StringDraw("Up : " + Input.Keys.toString(Settings.up), menux, menuy + 0 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Down : " + Input.Keys.toString(Settings.down), menux, menuy + 1 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Left : " + Input.Keys.toString(Settings.left), menux, menuy + 2 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Right : " + Input.Keys.toString(Settings.right), menux, menuy + 3 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Sprint : " + Input.Keys.toString(Settings.right), menux, menuy + 5 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Invisible / Select : " + Input.Keys.toString(Settings.right), menux, menuy + 6 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Pause : " + Input.Keys.toString(Settings.pause), menux, menuy + 8 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Map : " + Input.Keys.toString(Settings.map), menux, menuy + 9 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Profile : " + Input.Keys.toString(Settings.profile), menux, menuy + 11 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("GodMode : " + Input.Keys.toString(Settings.godMode), menux, menuy + 12 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Cheat1 : " + Input.Keys.toString(Settings.cheat1), menux, menuy + 13 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Cheat2 : " + Input.Keys.toString(Settings.cheat2), menux, menuy + 14 * offset, Color.WHITE, Game.fonts[0], z + 1, 4));
		s.setParent(this);
	}
}
