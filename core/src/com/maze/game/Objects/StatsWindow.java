package com.maze.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.maze.game.BaseObjects.GUIBox;
import com.maze.game.BaseObjects.GUITile;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.Game;
import com.maze.game.Stages.StartMenu;
import com.maze.game.Time;

import java.util.ArrayList;

/**
 * Created by zachm on 10/26/2017.
 */
public class StatsWindow extends GameObject {
	ArrayList<StringDraw> stats = new ArrayList<StringDraw>();
	Color[] colors = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE};
	float statColorTimer = 0;

	public StatsWindow() {
		super(0, 0, 0, 10);
		int menux = 145;
		int menuy = 0;
		int offset = 50;

		Game.addGameObject(new GUIBox(menux, menuy, 400, 250, z)).setParent(this);

		StringDraw s = (StringDraw) Game.addGameObject(new StringDraw("Deaths : " + GameplayTracker.deaths, menux, menuy + 2 * offset, Color.WHITE, Game.fonts[1], z + 1, 4));
		if (GameplayTracker.deathChallange) {
			stats.add(s);
		}
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Tiles Explored : " + GameplayTracker.tilesExplored, menux, menuy + offset, Color.WHITE, Game.fonts[1], z + 1, 4));
		if (GameplayTracker.tilesChallange) {
			stats.add(s);
		}
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Floors Fully Explored : " + GameplayTracker.floorsFullExplored, menux, menuy, Color.WHITE, Game.fonts[1], z + 1, 4));
		if (GameplayTracker.exploreChallange) {
			stats.add(s);
		}
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Floors Cleared : " + GameplayTracker.floorsCleared, menux, menuy - offset, Color.WHITE, Game.fonts[1], z + 1, 4));
		if (GameplayTracker.floorChallange) {
			stats.add(s);
		}
		s.setParent(this);
		s = (StringDraw) Game.addGameObject(new StringDraw("Games Won : " + GameplayTracker.gamesWon, menux, menuy - 2 * offset, Color.WHITE, Game.fonts[1], z + 1, 4));
		if (GameplayTracker.gameChallange) {
			stats.add(s);
		}
		s.setParent(this);
	}

	private Color starColor(int offset) {
		statColorTimer = ((StartMenu) Game.currentStage).starColorTimer;
		statColorTimer %= stats.size();
		int i = (int) statColorTimer;
		float d = statColorTimer - i;
		i = (i + offset) % stats.size();

		Color x = colors[i];
		Color y = colors[0];
		if (i < stats.size() - 1) {
			y = colors[i + 1];
		}
		float r = (1 - d) * (x.r * x.r) + d * (y.r * y.r);
		float g = (1 - d) * (x.g * x.g) + d * (y.g * y.g);
		float b = (1 - d) * (x.b * x.b) + d * (y.b * y.b);
		return new Color(r, g, b, 1.0f);
	}

	@Override
	public void update() {
		for (int i = 0; i < stats.size(); i++) {
			stats.get(i).color = starColor(i);
		}
	}
}
