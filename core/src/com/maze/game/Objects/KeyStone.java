package com.maze.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.Entity;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.Stages.MazeStage;


public class KeyStone extends Entity {
	int gatePair;
	MazeStage stage;
	public KeyStone(Vector2 pos, int value) {
		super(pos, 0, (int) -pos.y - 1, null);
		setSprite("Key.png");
		gatePair = value;
		stage = (MazeStage) Game.currentStage;
	}

	@Override
	public void update() {
		if (stage.player != null) {
			if (Compare.Vector(pos, stage.player.pos, stage.maze.nodeScaleWidth / 4, stage.maze.tileScaleHeight / 4)) {
				Delete();
				stage.player.inventory.add(new Float(gatePair));
			}
		}
	}
}
