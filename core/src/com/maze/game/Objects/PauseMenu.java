package com.maze.game.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.maze.game.BaseObjects.GUIBox;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.Game;
import com.maze.game.Settings;
import com.maze.game.Stages.FInalStage;
import com.maze.game.Stages.MazeStage;
import com.maze.game.Stages.StartMenu;
import com.maze.game.Time;

import java.awt.*;

/**
 * Created by zachm on 10/17/2017.
 */
public class PauseMenu extends GameObject {

	int selected = 0;
	int options = 4;
	StringDraw[] selectStrings = new StringDraw[options];
	StringDraw debugString = null;

	boolean resume = false;
	public PauseMenu(boolean resumeAllowed) {
		super(0, 0, 0, 10);

		if (resumeAllowed) {
			Game.addGameObject(new StringDraw("PAUSE", 0, 100, Color.WHITE, Game.fonts[2], z + 1, 1)).setParent(this);
		} else {
			if (Game.currentStage instanceof FInalStage) {
				Game.addGameObject(new StringDraw("YOU HAVE ESCAPED", 0, 100, Color.GREEN, Game.fonts[2], z + 1, 1)).setParent(this);
			} else {
				Game.addGameObject(new StringDraw("YOU HAVE DIED", 0, 100, Color.RED, Game.fonts[2], z + 1, 1)).setParent(this);
			}
		}
		GUIBox box = (GUIBox) Game.addGameObject(new GUIBox(0, 15, 100, 200, -10));
		box.setParent(this);

		if (!resumeAllowed) {
			options = 2;
			selectStrings = new StringDraw[options];
			box.Delete();
			selectStrings[0] = (StringDraw) Game.addGameObject(new StringDraw("Restart", 0, 0, Color.WHITE, Game.fonts[0], 10, 4));
			selectStrings[1] = (StringDraw) Game.addGameObject(new StringDraw("Exit", 0, -50, Color.WHITE, Game.fonts[0], 10, 4));
		} else {
			selectStrings[0] = (StringDraw) Game.addGameObject(new StringDraw("Resume", 0, 0, Color.WHITE, Game.fonts[0], 10, 4));
			debugString = selectStrings[1] = (StringDraw) Game.addGameObject(new StringDraw("Debug : F", 0, -25, Color.WHITE, Game.fonts[0], 10, 4));
			selectStrings[2] = (StringDraw) Game.addGameObject(new StringDraw("Restart", 0, -50, Color.WHITE, Game.fonts[0], 10, 4));
			selectStrings[3] = (StringDraw) Game.addGameObject(new StringDraw("Exit", 0, -75, Color.WHITE, Game.fonts[0], 10, 4));
		}

		for (int i = 0; i < options; i++) {
			selectStrings[i].setParent(this);
		}
		resume = resumeAllowed;
	}

	@Override
	public void update() {
		if (debugString != null) {
			if(Game.debug == true){
				debugString.str = "Debug : T";
			} else {
				debugString.str = "Debug : F";
			}
		}

		selectStrings[selected].color = Color.WHITE;
		if (Gdx.input.isKeyJustPressed(Settings.up)) {
			selected -= 1;
		}

		if (Gdx.input.isKeyJustPressed(Settings.down)) {
			selected += 1;
		}

		selected %= options;
		if (selected < 0) {
			selected += options;
		}
		selectStrings[selected].color = Color.SKY;

		if (resume) {
			if (Gdx.input.isKeyJustPressed(Settings.invisible)) {
				switch (selected) {
					case 0:
						Delete();
						break;
					case 1:
						Game.debug = !Game.debug;
						break;
					case 2:
						if (Game.debug) {
							if (Game.currentStage instanceof FInalStage) {
								Game.setStage(new FInalStage(100));
							} else {
								Game.setStage(new MazeStage(((MazeStage) Game.currentStage).currentLevel));
							}
						} else {
							Game.setStage(new MazeStage(0));
						}
						break;
					case 3:
						Game.setStage(new StartMenu());
						break;
				}
			}
		} else {
			if (Gdx.input.isKeyJustPressed(Settings.invisible)) {
				switch (selected) {
					case 0:
						Game.setStage(new MazeStage(0));
						break;
					case 1:
						Game.setStage(new StartMenu());
						break;
				}
			}
		}
	}
}
