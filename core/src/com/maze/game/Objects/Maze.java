package com.maze.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.BaseObjects.Tile;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.ResourceManager;

import java.util.*;


public class Maze {
	public static final int tileScaleWidth = 63;
	public static final int tileScaleHeight = 42;
	public static final int nodeScaleWidth = tileScaleWidth * 2;
	public static final int nodeScaleHeight = tileScaleHeight * 2;

	protected String floor = "Floor.png";
	protected String wallBase = "Wall/Maze/Wall";
	protected String wall = "Wall/Maze/Wall.png";
	protected String hideWall = "Wall/Maze/WallHidden.png";

	protected MazeNode[][] nodes;
	public  boolean[][] tileOpen;
	protected ArrayList<Tile> tiles = new ArrayList<Tile>();
	protected Tile[][] tileArray;

	public Vector2 startPoint;
	public Vector2 endPoint;
	public ArrayList<Vector2> endpoints = new ArrayList<Vector2>();
	public ArrayList<Vector2> nonEndPoints = new ArrayList<Vector2>();
	public ArrayList<Vector2> pitfallTiles = new ArrayList<Vector2>();
	public ArrayList<Vector2> gates = new ArrayList<Vector2>();
	public ArrayList<Vector2> keys = new ArrayList<Vector2>();
	protected ArrayList<GameObject> extraObjects = new ArrayList<GameObject>();
	public int width;
	public int height;
	public float gateProb;
	public float spikeProb;
	public float pitProb;
	public float breakChance;

	public Maze(int width, int height, float gateprob, float spikeProb, float pitProb, float breakC) {
		this.width = width;
		this.height = height;
		this.gateProb = gateprob;
		this.spikeProb = spikeProb;
		this.pitProb = pitProb;
		this.breakChance = breakC;
	}

	public void generateMaze() {
		destroyMaze();
		nodes = new MazeNode[width][height];
		tileArray = new Tile[width * 2 + 1][height * 2 + 1];
		endpoints.clear();
		gates.clear();
		keys.clear();

		for (GameObject o : extraObjects) {
			o.Delete();
		}
		extraObjects.clear();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				MazeNode n = new MazeNode(x, y);
				if (x > 0) {
					n.addNeighbor(nodes[x - 1][y]);
				}
				if (y > 0) {
					n.addNeighbor(nodes[x][y - 1]);
				}
				nodes[x][y] = n;
			}
		}

		tileOpen = new boolean[width*2 + 1][height*2 + 1];
		for (int x = 0; x < tileOpen.length; x++) {
			for (int y = 0; y < tileOpen[0].length; y++) {
				tileOpen[x][y] = false;
			}
		}

		depthFirstGeneration(nodes);

		for (int x = 0; x <= width; x++) {
			for (int y = 0; y <= height; y++) {
				//System.out.println(x * 2 + ", " + y * 2);
				if (y == height && x == width) {
					//System.out.println("both == width");
					int px = x * 2 - 1;
					int py = y * 2 - 1;
					//(int) -t.pos.y + tileScaleHeight / 2;
					Tile a = (Tile) Game.addGameObject(new Tile(wall, px * tileScaleWidth, py * tileScaleHeight, (int)((-py + 0.5) * tileScaleHeight)));
					tileArray[px + 1][py + 1] = a;
					tiles.add(a);
				} else if (x == width) {
					//System.out.println("x == width");
					int px = x * 2 - 1;
					int py = y * 2;
					Tile a = (Tile) Game.addGameObject(new Tile(wall, px * tileScaleWidth, (py - 1) * tileScaleHeight, (int)((-(py - 1) + 0.5) * tileScaleHeight)));
					Tile b = (Tile) Game.addGameObject(new Tile(wall, px * tileScaleWidth, py * tileScaleHeight, (int)((-py + 0.5) * tileScaleHeight)));
					tileArray[px + 1][py] = a;
					tileArray[px + 1][py + 1] = b;
					tiles.add(a); tiles.add(b);
				} else if (y == height) {
					//System.out.println("y == width");
					int px = x * 2;
					int py = y * 2 - 1;
					Tile a = (Tile) Game.addGameObject(new Tile(wall, (px - 1) * tileScaleWidth, py * tileScaleHeight, (int)((-py + 0.5) * tileScaleHeight)));
					Tile b = (Tile) Game.addGameObject(new Tile(wall, px * tileScaleWidth, py * tileScaleHeight, (int)((-py + 0.5) * tileScaleHeight)));
					tileArray[px][py + 1] = a;
					tileArray[px + 1][py + 1] = b;
					tiles.add(a); tiles.add(b);
				} else {
					spawnNode(nodes[x][y]);
				}
			}
		}

		for (int x = 0; x < width * 2 + 1; x++) {
			for (int y = 0; y < width * 2 + 1; y++) {
				Vector2 v = tileToPos(new Vector2(x, y));
				if (v != null) {
					//System.out.println(x + ", " + y + " : " + tileOpen[y][x]);
					if (!endpoints.contains(v) && tileOpen[x][y] && !gates.contains(v) && !keys.contains(v) && ! Compare.Vector(startPoint, v, 1)) {
						//System.out.println(x + ", " + y + " : " + tileOpen[y][x]);
						nonEndPoints.add(v);
					}
				}
			}
		}

		//System.out.println(nonEndPoints.size());
		//System.out.println(endpoints.size());

		endPoint = new Vector2(endpoints.get((int) (Game.random.nextFloat() * endpoints.size())));
		endpoints.remove(endPoint);

		randomBreak(breakChance);

		for (int i = 0; i < gates.size(); i++) {
			//System.out.println(gates.get(i));
			//System.out.println(keys.get(i));
			if (!Compare.Vector(gates.get(i), endPoint, 1)) {
				extraObjects.add(Game.addGameObject(new Gate(gates.get(i), i)));
				extraObjects.add(Game.addGameObject(new KeyStone(keys.get(i), i)));
			}
		}

		for (int x = 0; x < width * 2 + 1; x++) {
			for (int y = 0; y < width * 2 + 1; y++) {
				Vector2 v = tileToPos(new Vector2(x, y));
				if (nonEndPoints.contains(v)) {
					float f = Game.random.nextFloat();
					boolean placed = false;
					if (f <= spikeProb) {
						extraObjects.add(Game.addGameObject(new WallSpikes(v, 0, 0, null)));
						placed = true;
					}

					if (f <= spikeProb + pitProb && f > spikeProb) {
						extraObjects.add(Game.addGameObject(new PitfallTrap(v, 0, 0, null)));
						placed = true;
					}

					if (placed) {
						nonEndPoints.remove(tileToPos(new Vector2(x, y)));
						nonEndPoints.remove(tileToPos(new Vector2(x + 1, y)));
						nonEndPoints.remove(tileToPos(new Vector2(x - 1, y)));
						nonEndPoints.remove(tileToPos(new Vector2(x, y + 1)));
						nonEndPoints.remove(tileToPos(new Vector2(x, y - 1)));
					}
				}
			}
		}

		Vector2 startTile = posToTile(startPoint.x, startPoint.y);
		Vector2 endTile = posToTile(endPoint.x, endPoint.y);

		tiles.add((Tile) Game.addGameObject(new Tile("Enterance.png", startPoint.x, startPoint.y, (int) -(startPoint.y + tileScaleHeight - 1))));

		if (isTileOpen(new Vector2(endPoint).add(0, tileScaleHeight))) {
			tiles.add((Tile) Game.addGameObject(new Tile("ExitUp.png", endPoint.x, endPoint.y, (int) -(endPoint.y - tileScaleHeight / 2 + 1))));
		} else if (isTileOpen(new Vector2(endPoint).add(tileScaleWidth, 0))) {
			tiles.add((Tile) Game.addGameObject(new Tile("ExitRight.png", endPoint.x, endPoint.y, (int) -(endPoint.y - tileScaleHeight / 2 + 1))));
		} else if (isTileOpen(new Vector2(endPoint).add(0, -tileScaleHeight))) {
			tiles.add((Tile) Game.addGameObject(new Tile("ExitDown.png", endPoint.x, endPoint.y, (int) -(endPoint.y - tileScaleHeight / 2 + 1))));
		} else{
			tiles.add((Tile) Game.addGameObject(new Tile("ExitLeft.png", endPoint.x, endPoint.y, (int) -(endPoint.y - tileScaleHeight / 2 + 1))));
		}
	}

	protected void spawnNode(MazeNode m) {
		if (Game.debug) {
			System.out.print("Connected Node List for " + m + " : ");
			for (int i = 0; i < m.con.length; i++) {
				System.out.print(m.con[i] + ", ");
			}
			System.out.print("\n");
		}
		for (int x = -1; x <= 0; x++) {
			for (int y = -1; y <= 0; y++) {
				String res = floor;
				int pushDown = -tileScaleHeight;

				if (x != 0 && y != 0) {
					res = wall;
					pushDown = tileScaleHeight/2;
				}

				if ((x == 0 && y == 1) && (m.con[0] == null)) {
					res = wall;
					pushDown = tileScaleHeight/2;
				}

				if ((x == 0 && y == -1) && (m.con[2] == null)) {
					res = wall;
					pushDown = tileScaleHeight/2;
				}

				if ((x == 1 && y == 0) && (m.con[1] == null)) {
					res = wall;
					pushDown = tileScaleHeight/2;
				}

				if ((x == -1 && y == 0) && (m.con[3] == null)) {
					res = wall;
					pushDown = tileScaleHeight/2;
				}

				int px = nodeScaleWidth * m.xPos + tileScaleWidth * x;
				int py = nodeScaleHeight * m.yPos + tileScaleHeight * y;
				Tile t = new Tile(res, px, py, -py + pushDown);
				tiles.add(t);
				/*if (m.xPos * 2 + x + 1 == 1 && 1 == m.yPos * 2 + y + 1) {
					System.out.println("Add Tile " + px + ", " + py + " to array " + (m.xPos * 2 + x + 1) + ", " + (m.yPos * 2 + y + 1));
				}*/
				tileArray[m.xPos * 2 + x + 1][m.yPos * 2 + y + 1] = t;
				Game.drawObjects.add(t);

				if (res.compareTo(floor) == 0) {
					Vector2 tile = posToTile(px, py);
					int pdx = (int) tile.x;
					int pdy = (int) tile.y;
					tileOpen[pdx][pdy] = true;
					//System.out.println(px + ", " + py + " : " + pdx + ", " + pdy + "  Is Open : " + tileOpen[pdx][pdy]);
				} else {
					Vector2 tile = posToTile(px, py);
					int pdx = (int) tile.x;
					int pdy = (int) tile.y;
					tileOpen[pdx][pdy] = false;
					//System.out.println(px + ", " + py + " : " + pdx + ", " + pdy + "  Is Closed : " + tileOpen[pdx][pdy]);
				}
			}
		}
	}

	public void updateTileayout() {
		for (int x = 0; x < tileOpen.length; x++) {
			for (int y = 0; y < tileOpen[0].length; y++) {
				if (!tileOpen[x][y]) {
					boolean up = false;
					boolean right = false;
					boolean down = false;
					boolean left = false;

					if (x > 0) {
						left = !tileOpen[x - 1][y];
					}

					if (x < tileOpen.length - 1) {
						right = !tileOpen[x + 1][y];
					}

					if (y > 0) {
						down = !tileOpen[x][y - 1];
					}

					if (y < tileOpen[0].length - 1) {
						up = !tileOpen[x][y + 1];
					}

					String resName = "";
					if (up) {
						resName += "Up";
					}
					if (down) {
						resName += "Down";
					}
					if (left) {
						resName += "Left";
					}
					if (right) {
						resName += "Right";
					}

					tileArray[x][y].sprite.setTexture(ResourceManager.getImage(wallBase + resName + ".png"));
				}
			}
		}
	}

	private ArrayList<Tile> hiddenTiles = new ArrayList<Tile>();
	public void updateHidden(Vector2 pos) {
		if (hiddenTiles.size() > 0) {
			for (int i = 0; i < hiddenTiles.size(); i++) {
				if (!isTileOpen(hiddenTiles.get(i).pos)) {
					hiddenTiles.get(i).sprite.setTexture(ResourceManager.getImage(wall));
				}
			}
		}
		hiddenTiles.clear();
		if (pos != null) {
			Vector2 tilePos = posToTile(pos.x, pos.y);
			for (int x = -2; x <= 2; x++) {
				for (int y = -2; y <= 2; y++) {
					if (!isTileOpen(new Vector2(pos).add(x * tileScaleWidth, (y - 1) * tileScaleHeight)) && isTileOpen(new Vector2(pos).add(x * tileScaleWidth, y * tileScaleHeight))) {
						if (tilePos.x + x >= 1 && tilePos.y + y >= 1) {
							tileArray[(int) tilePos.x + x][(int) tilePos.y + y - 1].sprite.setTexture(ResourceManager.getImage(hideWall));
							hiddenTiles.add(tileArray[(int) tilePos.x + x][(int) tilePos.y + y - 1]);
						}
					}
				}
			}
			//System.out.println();
		}
	}

	public void randomBreak(float probability) {
		int maxX = width * 2;
		int maxY = height * 2;
		for (int x = 1; x < maxX; x++) {
			for (int y = 1; y < maxY; y++) {
				if (Game.random.nextFloat() <= probability) {
					breakTile(new Vector2(x, y), false);
				}
			}
		}
	}

	protected void depthFirstGeneration(MazeNode[][] nodes) {
		boolean forward = true;
		//Initializes visited array, the path linked list, and gets current node
	 	boolean[][] visited = new boolean[width][height];
		LinkedList<MazeNode> path = new LinkedList<MazeNode>();

		MazeNode currentNode = nodes[(int) (Game.random.nextFloat() * width)][(int) (Game.random.nextFloat() * height)];

		path.add(currentNode);
		startPoint = (nodeToPos(path.peek()));
		while(path.peek() != null) {
			currentNode = path.peek();
			visited[currentNode.xPos][currentNode.yPos] = true;

			MazeNode[] neighbors = new MazeNode[currentNode.neighbors.size()];
			currentNode.neighbors.toArray(neighbors);
			ArrayList<MazeNode> dest = new ArrayList<MazeNode>();

			for (int i = 0; i < neighbors.length; i++) {
				MazeNode n = neighbors[i];
				//System.out.println(n.xPos + ", " + n.yPos);
				if (!visited[n.xPos][n.yPos]) {
					dest.add(n);
				}
			}
			if (Game.debug) {
				System.out.print("Destinations for " + currentNode + "(" + dest.size() + ")" + " : ");
				for (int i = 0; i < dest.size(); i++) {
					System.out.print(dest.get(i) + ", ");
				}
				System.out.print("\n");
			}

			if (dest.size() > 0) {
				// connects current to next node and adds next to beginning of list
				forward = true;
				MazeNode nextNode = dest.get((int) (Game.random.nextFloat() * dest.size()));
				if (Game.random.nextFloat() <= gateProb && endpoints.size() >= 1) {
					gates.add(nodeToPos(nextNode));
					Vector2 point = endpoints.get((int) Game.random.nextFloat() * endpoints.size());
					keys.add(point);
					endpoints.remove(point);
				}
				if (nextNode.yPos > currentNode.yPos) {
					currentNode.connect(nextNode, 'u');
				}

				if (nextNode.yPos < currentNode.yPos) {
					currentNode.connect(nextNode, 'd');
				}

				if (nextNode.xPos > currentNode.xPos) {
					currentNode.connect(nextNode, 'r');
				}

				if (nextNode.xPos < currentNode.xPos) {
					currentNode.connect(nextNode, 'l');
				}

				path.addFirst(nextNode);
			} else {
				//pulls current node off list if all neighbors visited
				if (forward) {
					forward = false;
					endpoints.add(nodeToPos(path.peek()));
				}
				path.pop();

			}

		}

	}

	public boolean inMaze(Vector2 pos) {
		Vector2 tile = posToTile(pos.x, pos.y);
		//System.out.print(pos + " -> " + (int) tile.x + ", " + (int) tile.y);
		if (tile.x >= 0 && tile.x < tileOpen.length) {
			if (tile.y >= 0 && tile.y < tileOpen[0].length) {
				return true;
			}
		}
		return false;
	}

	public boolean rayCastArea(Vector2 src, Vector2 dst, int maxLen) {
		return rayCast(src, dst, maxLen) || rayCast(src, new Vector2(dst).add(-tileScaleWidth, 0), maxLen) || rayCast(src, new Vector2(dst).add(+tileScaleWidth, 0), maxLen) || rayCast(src, new Vector2(dst).add(0, +tileScaleHeight), maxLen) || rayCast(src, new Vector2(dst).add(0, -tileScaleHeight), maxLen);
	}

	public boolean rayCast(Vector2 src, Vector2 dst) {
		return rayCast(src, dst, Integer.MAX_VALUE);
	}

	public boolean rayCast(Vector2 src, Vector2 dst, int maxLen) {
		Vector2 tileSource = posToTile(src.x, src.y);
		Vector2 tileDest = posToTile(dst.x, dst.y);

		if ((int) tileSource.x == (int) tileDest.x && (int) tileSource.y == (int) tileDest.y) {
			return isTileOpen(tileToPos(tileSource));
		} else if ((int) tileSource.x == (int) tileDest.x) {
			int offset =  (int) tileDest.y - (int)tileSource.y;
			if (Math.abs(offset) >= maxLen) {
				return false;
			}
			if (offset > 0) {
				offset = 1;
			} else {
				offset = -1;
			}
			boolean retVal = isTileOpen(tileSource);
			while ((int) tileSource.y != (int) tileDest.y) {
				tileSource.y += offset;
				retVal = retVal && isTileOpen(tileToPos(tileSource));
			}
			return retVal;
		} if ((int) tileSource.y == (int) tileDest.y) {
			int offset = (int) tileDest.x - (int)tileSource.x;
			if (Math.abs(offset) >= maxLen) {
				return false;
			}
			if (offset > 0) {
				offset = 1;
			} else {
				offset = -1;
			}
			boolean retVal = isTileOpen(tileToPos(tileSource));
			while ((int) tileSource.x != (int) tileDest.x) {
				tileSource.x += offset;
				retVal = retVal && isTileOpen(tileToPos(tileSource));
			}
			return retVal;
		}



		return false;
	}

	public boolean isTileOpen(Vector2 pos) {
		if (pos == null) {
			return  false;
		}
		Vector2 tile = posToTile(pos.x, pos.y);
		//System.out.print(pos + " -> " + (int) tile.x + ", " + (int) tile.y);
		if (tile.x >= 0 && tile.x < tileOpen.length) {
			if (tile.y >= 0 && tile.y < tileOpen[0].length) {
				return tileOpen[(int)tile.x][(int)tile.y];
			}
		}
		return false;
	}

	public MazeNode posToNode(float x, float y) {
		Vector2 tile = posToTile(x, y);
		//System.out.println(x + ", " + y + " -> " + (int) tile.x + ", " + (int) tile.y);
		int nodex = (int) (tile.x / 2);
		int nodey = (int) (tile.y / 2);
		//System.out.println(nodex + ", " + nodey);
		if (nodex >= 0 && nodex  < nodes.length) {
			if (nodey >= 0 && nodey  < nodes[0].length) {
				return nodes[nodex][nodey];
			}
		}
		return null;
	}

	public Vector2 nodeToPos(MazeNode m) {
		Vector2 pos = new Vector2(m.xPos, m.yPos);
		pos.x *= nodeScaleWidth;
		pos.y *= nodeScaleHeight;
		return pos;
	}

	public Vector2 nodeToPos(int x, int y) {
		Vector2 pos = new Vector2(x, y);
		pos.x *= nodeScaleWidth;
		pos.y *= nodeScaleHeight;
		return pos;
	}

	public Vector2 pushBack(Vector2 pos) {
		Vector2 tile = posToTile(pos.x, pos.y);
		Vector2 push = new Vector2();

		push.y = 1;

		return push.nor();
	}

	public Vector2 posToTile(float x, float y) {
		float fTileX = 0.5f + x / tileScaleWidth;
		float fTileY = 0.5f + y / tileScaleHeight;
		if (fTileX < 0) {
			fTileX -= 1;
		}
		if (fTileY < 0) {
			fTileY -= 1;
		}
		int tileX = (int) fTileX + 1;
		int tileY = (int) fTileY + 1;
		return new Vector2(tileX, tileY);
	}

	public Vector2 tileToPos(Vector2 tile) {
		//System.out.println(tile);
		//System.out.println(tileArray[(int) tile.x][(int) tile.y].pos);
		if (tile.x < 0 | tile.x > tileArray.length - 1) {
			return  null;
		}
		if (tile.y < 0 | tile.y > tileArray[0].length - 1) {
			return  null;
		}
		//System.out.println(tile);
		return tileArray[(int) tile.x][(int) tile.y].pos;
	}

	public void destroyMaze() {
		for (Tile t : tiles) {
			Game.drawObjects.remove(t);
		}
		tiles.clear();
		nodes = null;
	}


	private Comparator<PathNode> pathComp = new Comparator<PathNode>() {
		@Override
		public int compare(PathNode o1, PathNode o2) {
			return  o1.cost + o1.h - o2.cost + o2.h;
		}
	};

	private boolean compare(Vector2 tile, Vector2 tile2) {
		if ((int) tile.x == (int) tile2.x && (int) tile.y == (int) tile2.y) {
			return true;
		}
		return  false;
	}

	private int dist(Vector2 n, Vector2 m) {
		int dx = (int) (n.x - m.x);
		int dy = (int) (n.y - m.x);
		return dx * dx + dy * dy;
	}

	public void breakTile(Vector2 pos, boolean screenCoord) {
		if (pos != null) {
			Vector2 tilePos;
			if (screenCoord) {
				tilePos = posToTile(pos.x, pos.y);
			}  else {
				tilePos = new Vector2(pos);
			}
			if ((tilePos.x >= 0 && tilePos.x < tileArray.length) && (tilePos.y >= 0 && tilePos.y < tileArray[0].length)) {
				Tile t = tileArray[(int) tilePos.x][(int) tilePos.y];
				t.sprite.setTexture(ResourceManager.getImage(floor));
				t.z = (int) -t.pos.y - tileScaleHeight;
				tileOpen[(int) tilePos.x][(int) tilePos.y] = true;
			}
		}
	}

	public void buildTile(Vector2 pos, boolean screenCoord) {
		if (pos != null) {
			Vector2 tilePos;
			if (screenCoord) {
				tilePos = posToTile(pos.x, pos.y);
			}  else {
				tilePos = new Vector2(pos);
			}
			if ((tilePos.x >= 0 && tilePos.x < tileArray.length) && (tilePos.y >= 0 && tilePos.y < tileArray[0].length)) {
				Tile t = tileArray[(int) tilePos.x][(int) tilePos.y];
				if (t != null) {
					t.sprite.setTexture(ResourceManager.getImage(wall));
					t.z = (int) -t.pos.y + tileScaleHeight / 2;
					tileOpen[(int) tilePos.x][(int) tilePos.y] = false;
				}
			}
		}
	}

	public Vector2[] pathFind(Vector2 src, Vector2 dst) {
		Hashtable<String, PathNode> traveledNodes = new Hashtable<String, PathNode>();
		ArrayList<PathNode> nodeList = new ArrayList<PathNode>();

		if (src == null | dst == null) {
			Vector2[] path = {dst};
			return null;
		}

		if (!isTileOpen(src) | !isTileOpen(dst)) {
			//return null;
		}

		Vector2 start = posToTile(src.x, src.y);
		Vector2 end = posToTile(dst.x, src.y);
		PathNode endPath = null;
		Vector2[] returnPath = null;
		ArrayList<Vector2> returnPathArrayList = new ArrayList<Vector2>();

		Vector2 startTile = posToTile(src.x, src.y);
		Vector2 endTile = posToTile(dst.x, dst.y);

		if (compare(startTile, endTile)) {
			Vector2[] path = {src, tileToPos(endTile)};
			return  path;
		}

		//System.out.println("Source of path " + src + " : " + start);
		//System.out.println("End of path " + dst + " : " + end);

		nodeList.add(new PathNode(new Vector2(startTile), 1, dist(start, end)));
		traveledNodes.put(nodeList.get(0).toString(), nodeList.get(0));

		//System.out.println("StartTile : " + startTile + " , EndTile :" + endTile);
		Vector2[] offsets = {new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, -1), new Vector2(-1, 0)};
		while (nodeList.size() > 0 && endPath == null) {
			PathNode currentNode = nodeList.get(0);
			nodeList.remove(currentNode);

			//System.out.println(currentNode.mazeNode);

			//System.out.println("Current Tile : " + currentNode);
			for (int i = 0; i < 4; i++) {
				Vector2 nextTile = new Vector2(currentNode.tile).add(offsets[i]);
				//System.out.println("Offset : " + offset + "Next Tile :" + nextTile);
				if (nextTile.x > 0 && nextTile.y > 0 && nextTile.x < tileOpen.length && nextTile.y < tileOpen[0].length && tileOpen[(int) nextTile.x][(int) nextTile.y]) {
					PathNode nextNode = new PathNode(new Vector2(currentNode.tile).add(offsets[i]), currentNode.cost + 1, dist(start, end));
					if (pitfallTiles.contains(nextNode.tile)) {
						nextNode.cost += 50;
					}
					nextNode.parent = currentNode;
					//System.out.println(nextNode + " : " + endTile);
					if (Compare.Vector(endTile, nextTile, 1)) {
						endPath = nextNode;
						//System.out.println("Path Found : " + nextNode);
					}
					if (!traveledNodes.containsKey(nextNode.toString())) {
						traveledNodes.put(nextNode.toString(), nextNode);
						nodeList.add(nextNode);
						//System.out.println("Parent of " + nextNode + " is : " + nextNode.parent);
					}
				}
			}
			Collections.sort(nodeList, pathComp);
		}


		if (endPath != null) {
			Vector2 currentPoint = tileToPos(endPath.tile);
			Vector2 nextPoint = tileToPos(endPath.parent.tile);
			while (endPath.parent != null) {
				//System.out.println(endPath.mazeNode);

				returnPathArrayList.add(tileToPos(endPath.tile));
				endPath = endPath.parent;
				if (endPath != null) {
					currentPoint = nextPoint;
					nextPoint = tileToPos(endPath.tile);
				}
			}
			returnPathArrayList.add(src);
		} else {
			//System.out.println("No Path Detected");
		}


		//System.out.println("Path");
		if (returnPathArrayList.size() > 0) {
			returnPath = new Vector2[returnPathArrayList.size()];
			for (int i = 0; i < returnPathArrayList.size(); i++) {
				//System.out.println(returnPathArrayList.get(returnPathArrayList.size() - 1 - i));
				returnPath[i] = new Vector2(returnPathArrayList.get(returnPathArrayList.size() - 1 - i));
			}
		}

		if (Game.debug) {
			//printPath(returnPath);
			debugPath(returnPath);
		}
		return returnPath;
	}

	public void debugPath(Vector2[] path) {
		if (path != null) {
			for (int i = 0; i < path.length - 1; i++) {
				Game.addDebugLine(path[i].x, path[i].y, path[i + 1].x, path[i + 1].y);
			}
		}
	}

	public void printPath(Vector2[] path) {
		if (path != null) {
			System.out.println("Path : ");
			for (int i = 0; i < path.length; i++) {
				System.out.print(path[i] + " : ");
				System.out.println(posToTile(path[i].x, path[i].y));
			}
		} else {
			System.out.println("No Path");
		}
	}
}

class PathNode{
	public PathNode parent;
	public Vector2 tile;

	public int cost;
	public int h;

	public PathNode(Vector2 tile, int cost, int h) {
		this.tile = tile;
		this.cost = cost;
		this.h = h;
	}

	@Override
	public String toString() {
		return tile.toString();
	}
}
