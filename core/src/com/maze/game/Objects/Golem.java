package com.maze.game.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.AnimatedEntity;
import com.maze.game.BaseObjects.DrawLight;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Stages.MazeStage;
import com.maze.game.Time;

/**
 * Created by zachm on 10/8/2017.
 */
public class Golem extends AnimatedEntity implements DrawLight {
	Animation[] idle = new Animation[4];
	Animation[] walk = new Animation[4];
	Animation attack;
	int dir = 0;
	float speed = 100;
	float maxSpeed = 600;
	float minSpeed = 100;
	float acceleration = 50;
	Sound[] footsteps = new Sound[3];

	float persuitTimer = 0;
	float soundTime = 0;

	int side = 0;
	Vector2[] path;
	Sprite lightSprite;
	float lightSize = 0.5f;
	Vector2[] patrol;
	int patrolIndex = 0;
	Vector2 dest = new Vector2();
	public boolean attackingTile = false;
	float attackTime = 0.75f;
	float currentAttack = 0;

	MazeStage stage;

	public Golem(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);

		idle[0] = ResourceManager.getAnim("Golem/Idle/IdleUp.png", 60, 1 / 60f);
		idle[1] = ResourceManager.getAnim("Golem/Idle/IdleRight.png", 60, 1 / 60f);
		idle[2] = ResourceManager.getAnim("Golem/Idle/IdleDown.png", 60, 1 / 60f);
		idle[3] = ResourceManager.getAnim("Golem/Idle/IdleLeft.png", 60, 1 / 60f);

		walk[0] = ResourceManager.getAnim("Golem/Walk/WalkUp.png", 70, 1 / 60f);
		walk[1] = ResourceManager.getAnim("Golem/Walk/WalkRight.png", 70, 1 / 60f);
		walk[2] = ResourceManager.getAnim("Golem/Walk/WalkDown.png", 70, 1 / 60f);
		walk[3] = ResourceManager.getAnim("Golem/Walk/WalkLeft.png", 70, 1 / 60f);

		attack = ResourceManager.getAnim("Golem/Hit/Hit.png", 47, 1 / 20f);

		footsteps[0] = ResourceManager.getSound("Sounds/Feets/Step01.wav");
		footsteps[1] = ResourceManager.getSound("Sounds/Feets/Step02.wav");
		footsteps[2] = ResourceManager.getSound("Sounds/Feets/Step03.wav");

		setAnim(idle[2], 0, "idle");
		lightSprite = new Sprite(ResourceManager.getImage("light.png"));
		patrol = new Vector2[2];
		if (Game.currentStage instanceof MazeStage) {
			stage = (MazeStage) Game.currentStage;
			if (stage.maze.endpoints.size() > 1) {
				patrol[1] = stage.maze.endpoints.get((int) (Game.random.nextFloat() * stage.maze.endpoints.size()));
				stage.maze.endpoints.remove(patrol[1]);
			} else {
				patrol[1] = stage.maze.nodeToPos((int) (Game.random.nextFloat() * stage.maze.width), (int) (Game.random.nextFloat() * stage.maze.height));
			}
		}
		patrol[0] = new Vector2(localPos);
		setPath(patrol[1]);

		if (path != null && !Compare.Vector(path[path.length - 1], patrol[1], 1)) {
			patrol[1].set(path[path.length - 1]);
		}
	}

	private void setPath(Vector2 dest) {
		if (! attackingTile && stage.maze.posToTile(dest.x, dest.y) != null && stage.maze.tileToPos(stage.maze.posToTile(dest.x, dest.y)) != null) {
			this.dest.set(stage.maze.tileToPos(stage.maze.posToTile(dest.x, dest.y)));
			path = stage.maze.pathFind(pos, dest);
			if (path != null && path.length == 1 && Compare.Vector(pos, path[0], 2 * speed * Time.deltaTime)) {
				pos.set(path[0]);
				path = null;
			}
		}
	}

	@Override
	public void update() {
		if (stage.player == null) return;

		if (!attackingTile) {
			if ((!stage.player.invisible && stage.maze.rayCast(pos, stage.player.pos)) | (stage.player.running && Compare.Vector(pos, stage.player.pos, 150))) {
				persuitTimer = 1;
				setPath(stage.player.pos);
			} else if (Compare.Vector(localPos, dest, speed * 2 * Time.deltaTime) && persuitTimer <= 0) {
				patrolIndex += 1;
				patrolIndex %= patrol.length;
				setPath(patrol[patrolIndex]);
			} else {
				setPath(dest);
			}

			if (path != null) {
				Vector2 dist = new Vector2(path[1]);
				dist.sub(path[0]);
				velocity.set(dist);

				if (path.length > 1) {
					if (Math.abs(path[1].x - path[0].x) > Math.abs(path[1].y - path[0].y)) {
						if (!Compare.Float(path[1].y, path[0].y, speed * Time.deltaTime)) {
							velocity.set(0, path[1].y - path[0].y);
						} else {
							localPos.y = path[1].y;
						}
					} else {
						if (!Compare.Float(path[1].x, path[0].x, speed * Time.deltaTime)) {
							velocity.set(path[1].x - path[0].x, 0);
						} else {
							localPos.x = path[1].x;
						}
					}
				}

				if (persuitTimer > 0) {
					speed += acceleration * Time.deltaTime;
				} else {
					speed -= 8 * attackTime * Time.deltaTime;
				}

				if (speed > maxSpeed) {
					speed = maxSpeed;
				}

				if (speed < minSpeed) {
					speed = minSpeed;
				}

				velocity.nor().scl(speed);
				//System.out.println(persuitTimer > 1 && Compare.Vector(pos, dest, speed * Time.deltaTime));
				if (persuitTimer > 0 && Compare.Vector(pos, dest, speed * Time.deltaTime)) {
					localPos.set(dest);
					attackingTile = true;
					currentAttack = 0;
					velocity.set(0, 0);
					//System.out.println("Attack!");
				}
			} else {
				velocity.set(0, 0);
			}

			if (!stage.maze.isTileOpen(pos)) {
				velocity.set(0, 0);
			}

			if (stage.maze.pitfallTiles.contains(stage.maze.posToTile(pos.x, pos.y))) {
				velocity.set (0, 0);
			}

			if (Math.abs(velocity.y) > Math.abs(velocity.x)) {
				if (velocity.y > 0) {
					dir = 0;
				}
				if (velocity.y < 0) {
					dir = 2;
				}
			} else {
				if (velocity.x > 0) {
					dir = 1;
				}
				if (velocity.x < 0) {
					dir = 3;
				}
			}
		} else {
			velocity .set(0, 0);
			currentAttack += Time.deltaTime;

			if (stage.maze.rayCast(pos, stage.player.pos) && !stage.player.invisible && !Compare.Vector(stage.maze.posToTile(pos.x, pos.y), stage.maze.posToTile(stage.player.pos.x, stage.player.pos.y), 1)) {
				currentAttack = 0;
				attackingTile = false;
			}

			if (currentAttack >= attackTime) {
				if (stage.player != null && Compare.Vector(stage.maze.posToTile(pos.x, pos.y), stage.maze.posToTile(stage.player.pos.x, stage.player.pos.y), 1)) {
					stage.player.kill();
				}
				persuitTimer = 0;
				currentAttack = 0;
				attackingTile = false;
			}

		}
	}

	@Override
	public void lateUpdate() {
		super.lateUpdate();
		z = (int) -pos.y;

		if (attackingTile) {
			setAnim(attack, stateTime, "attack");
		} else {

			if (idle != null) {
				if (velocity.x == 0 && velocity.y == 0) {
					setAnim(idle[dir], stateTime, "idle");
				} else {
					setAnim(walk[dir], stateTime, "walk");
				}
			}

			if (persuitTimer > 0) {
				color = Color.RED;
			} else {
				color = Color.WHITE;
			}

			if (velocity != null) {
				if (velocity.x != 0 | velocity.y != 0) {
					if (soundTime > 0.5f) {
						soundTime = 0;
					}
					soundTime -= Time.deltaTime * speed / minSpeed;
				} else {
					soundTime = 100;
				}
			}
			if (soundTime <= 0) {
				Game.addGameObject(new SoundWave(pos));
				if (footsteps != null && stage.player != null) {
					float pan = (pos.x - stage.player.pos.x) / 100f;
					if (pan < -1) {
						pan = -1;
					}
					if (pan > 1) {
						pan = 1;
					}
					float vol = 2;
					vol -= new Vector2(pos).sub(stage.player.pos).len() / 250f;
					if (vol < 0) {
						vol = 0;
					}
					//System.out.println("Step : " + vol + ", " + pan);
					footsteps[(int) (Game.random.nextFloat() * footsteps.length)].play(vol, 1, pan);
				}
				soundTime = 0.5f;
			}

			if (stage != null && stage.player != null && (!stage.player.againstWall) && Compare.Vector(pos, stage.player.pos, 40, 30)) {
				stage.player.kill();
			}
		}
	}

	@Override
	public void renderLight(Batch graphics) {
		if (stage.player != null && !stage.player.invisible) {
			float dist = new Vector2(pos).sub(stage.player.pos).len();
			float a = 255 - dist / 2;
			//System.out.println(a);
			lightSprite.setColor(new Color(0.9f, 0.9f, 1, a/255f));
			lightSprite.setCenter(pos.x, pos.y);
			lightSprite.setRotation(rot);
			lightSprite.setScale(lightSize);
			lightSprite.draw(graphics);
		}
	}

	@Override
	public int lightz() {
		return 1;
	}
}
