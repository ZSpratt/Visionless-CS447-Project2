package com.maze.game.Objects;


import com.maze.game.Game;

import java.util.ArrayList;

public class MazeNode {
	protected MazeNode[] con = new MazeNode[4];
	protected int xPos;
	protected int yPos;
	protected MazeNode[] connected;
	protected ArrayList<MazeNode> neighbors = new ArrayList<MazeNode>();

	public MazeNode(int xPos, int yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}

	public void addNeighbor(MazeNode other) {
		neighbors.add(other);
		other.neighbors.add(this);
		if (Game.debug) {
			System.out.println("neighboring " + xPos + ", " + yPos + " to " + other.xPos + ", " + other.yPos);
		}
	}
	public void connect(MazeNode other, char dir) {
		if (Game.debug) {
			System.out.println("Connecting " + xPos + ", " + yPos + " to " + other.xPos + ", " + other.yPos + " on " + dir);
		}
		switch (dir){
			case 'u':
				con[0] = other;
				other.con[2] = this;
				break;
			case 'd':
				con[2] = other;
				other.con[0] = this;
				break;
			case 'l':
				con[3] = other;
				other.con[1] = this;
				break;
			case 'r':
				con[1] = other;
				other.con[3] = this;
				break;
		}

		updateConnected();
		other.updateConnected();
	}

	public void updateConnected() {
		int count = 0;
		for (int i = 0; i < con.length; i++) {
			if (con[i] != null) {
				count += 1;
			}
		}

		if (count >= 1) {
			connected = new MazeNode[count];
			int ind = 0;
			for (int i = 0; i < con.length; i++) {
				if (con[i] != null) {
					connected[ind] = con[i];
					ind += 1;
				}
			}
		}
	}

	@Override
	public String toString() {
		return xPos + ", " + yPos;
	}
}
