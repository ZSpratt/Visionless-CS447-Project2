package com.maze.game.Objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.AnimatedEntity;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Stages.MazeStage;
import com.maze.game.Time;

/**
 * Created by zachm on 10/20/2017.
 */
public class PitfallTrap extends AnimatedEntity {

	boolean breaking;
	boolean broken;
	float fixTime = 0;

	MazeStage stage;
	public PitfallTrap(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		setAnim(ResourceManager.getAnim("Traps/FloorCrumble.png", 49, 1/120f), 0);
		anim.setPlayMode(Animation.PlayMode.NORMAL);

		stage = (MazeStage) Game.currentStage;
		this.z = (int) (-pos.y - stage.maze.tileScaleHeight + 1);
	}

	@Override
	public void update() {
		if (stage == null | stage.player == null) {
			return;
		}
		//System.out.println(pos + " : " + stateTime + "  Broken, Breaking : " + broken + ", " + breaking);
		if (!broken) {
			if (!breaking) {
				stateTime = 0;
				if (Compare.Vector(pos, stage.player.pos, 56/2, 40/2)) {
					breaking = true;
					fixTime = 0;
					stage.maze.pitfallTiles.add(stage.maze.posToTile(pos.x, pos.y));
				}
			} else {
				stateTime += Time.deltaTime;
				if (anim.isAnimationFinished(stateTime)) {
					broken = true;
					stateTime -= Time.deltaTime;
				}
			}
		} else {
			if (Compare.Vector(pos, stage.player.pos, 56/2, 40/2)) {
				stage.player.kill();
			}
			fixTime += Time.deltaTime;
			 if (fixTime >= 10) {
			 	broken = false;
			 	breaking = false;
			 	stateTime = 0;
			 	stage.maze.pitfallTiles.remove(stage.maze.posToTile(pos.x, pos.y));
			 }
		}
	}

	@Override
	public void render(Batch batch) {
		if (!anim.isAnimationFinished(stateTime) |       looping) {
			currentFrame = new Sprite((TextureRegion) anim.getKeyFrame(stateTime, looping));

			currentFrame.setRotation(rot);
			currentFrame.setCenter(pos.x + offset.x, pos.y + offset.y);
			currentFrame.setColor(color);
			currentFrame.draw(batch);
		}
	}

	@Override
	public int z() {
		return z;
	}
}
