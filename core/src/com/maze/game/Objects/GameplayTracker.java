package com.maze.game.Objects;


import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Game;
import com.maze.game.Stages.FInalStage;
import com.maze.game.Stages.MazeStage;

public class GameplayTracker extends GameObject{
	public static int deaths;
	public static boolean deathChallange;
	public static int tilesExplored;
	public static boolean tilesChallange;
	public static int floorsFullExplored;
	public static boolean exploreChallange;
	public static int floorsCleared;
	public static boolean floorChallange;
	public static int gamesWon;
	public static boolean gameChallange;
	public static boolean challangesCompleted;
	public static int numChallange;

	boolean resultsValid = true;
	MazeStage stage;
	int currentTiles;

	public GameplayTracker() {
		super(0, 0, 0, 0);
		if (Game.currentStage instanceof MazeStage) {
			stage = (MazeStage) Game.currentStage;
		}
	}

	@Override
	public void update() {
		if (Game.debug) {
			resultsValid = false;
		}
		if (stage.player == null) {
			Delete();
		}
		currentTiles = stage.map.exploredTiles;
	}

	@Override
	public void onDelete() {
		numChallange = 0;
		if (!Game.debug && resultsValid && stage != null) {
			tilesExplored += currentTiles;
			if (stage.player == null) {
				deaths += 1;
			} else {
				if (stage instanceof FInalStage) {
					gamesWon += 1;
				} else {
					floorsCleared += 1;
				}
			}
			if (stage.map.fullyExplored) {
				floorsFullExplored += 1;
			}
		}

		deathChallange = (deaths >= 10);
		tilesChallange = (tilesExplored >= 1000);
		floorChallange = (floorsCleared >= 10);
		exploreChallange = (floorsFullExplored >= 3);
		gameChallange = (gamesWon >= 1);
		if (deathChallange)
			numChallange ++;
		if (tilesChallange)
			numChallange ++;
		if (floorChallange)
			numChallange ++;
		if (exploreChallange)
			numChallange ++;
		if (gameChallange)
			numChallange ++;
		challangesCompleted = (deathChallange && tilesChallange && floorChallange && gameChallange && exploreChallange);

		//System.out.println(this);
		Game.saveGame();
	}

	@Override
	public String toString() {
		return String.format("deaths : %d\ntiles explored : %d\nfloors cleared : %d\ngames won : %d\n", deaths, tilesExplored, floorsCleared, gamesWon);
	}
}
