package com.maze.game.Objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.Entity;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.Tile;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Stages.MazeStage;


public class WallSpikes extends Entity {

	//Sprite[] spikes;
	Vector2[] offsets;
	Vector2[] tiles;
	MazeStage stage;

	Tile[] tileDraws;

	public WallSpikes(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		setSprite("Traps/WallSpikesUp.png");
		/*
		spikes = new Sprite[4];
		spikes[0] = new Sprite(ResourceManager.getImage("Traps/WallSpikesDown.png"));
		spikes[1] = new Sprite(ResourceManager.getImage("Traps/WallSpikesRight.png"));
		spikes[2] = new Sprite(ResourceManager.getImage("Traps/WallSpikesUp.png"));
		spikes[3] = new Sprite(ResourceManager.getImage("Traps/WallSpikesLeft.png"));
		*/
		stage = (MazeStage) Game.currentStage;

		offsets = new Vector2[4];
		offsets[0] = new Vector2(0, 1);
		offsets[1] = new Vector2(-1, 0);
		offsets[2] = new Vector2(0, -1);
		offsets[3] = new Vector2(1, 0);

		tiles = new Vector2[4];
		tiles[0] = stage.maze.posToTile(pos.x, pos.y).add(0, 1);
		tiles[1] = stage.maze.posToTile(pos.x, pos.y).add(-1, 0);
		tiles[2] = stage.maze.posToTile(pos.x, pos.y).add(0, -1);
		tiles[3] = stage.maze.posToTile(pos.x, pos.y).add(1, 0);

		this.z = (int) (-pos.y);

		tileDraws = new Tile[4];
		tileDraws[0] = new Tile("Traps/WallSpikesDown.png", pos.x, pos.y, this.z - stage.maze.tileScaleHeight / 2);
		tileDraws[1] = new Tile("Traps/WallSpikesRight.png", pos.x, pos.y, this.z);
		tileDraws[2] = new Tile("Traps/WallSpikesUp.png", pos.x, pos.y, this.z + stage.maze.tileScaleHeight / 2);
		tileDraws[3] = new Tile("Traps/WallSpikesLeft.png", pos.x, pos.y, this.z);
	}

	@Override
	public void update() {
		if (stage.player != null && !delete) {
			Vector2 delta = new Vector2(stage.player.pos).sub(pos);
			//System.out.println(delta);
			for (int i = 0; i < 4; i++) {
				boolean draw = true;
				if (!stage.maze.tileOpen[(int) tiles[i].x][(int) tiles[i].y] && !stage.maze.gates.contains(stage.maze.tileToPos(new Vector2((int) tiles[i].x, (int) tiles[i].y)))) {
					if (offsets[i].x > 0 && delta.x > 24 || offsets[i].x < 0 && delta.x < -24 || offsets[i].y > 0 && delta.y > 17 || offsets[i].y < 0 && delta.y < -17) {
							if (Compare.Vector(stage.maze.posToTile(stage.player.pos.x, stage.player.pos.y), stage.maze.posToTile(pos.x, pos.y), 0.5f)) {
							//System.out.println(offsets[i]);
							//System.out.println("Die");
							stage.player.kill();
						}
					}
				} else {
					draw = false;
				}

				boolean contains = Game.drawObjects.contains(tileDraws[i]);
				if (draw && !contains) {
					Game.drawObjects.add(tileDraws[i]);
				} else if (!draw && contains){
					Game.drawObjects.remove(tileDraws[i]);
				}
				//System.out.println();
			}
		}
	}

	@Override
	public void render(Batch batch) {
		/*
		for (int i = 0; i < 4; i++) {
			if (!stage.maze.tileOpen[(int) tiles[i].x][(int) tiles[i].y]) {
				spikes[i].setCenter(pos.x, pos.y);
				spikes[i].setColor(color);
				spikes[i].draw(batch);
			}
		}*/
	}

	@Override
	public void onDelete() {
		for (int i = 0; i < 4; i++) {
			Game.drawObjects.remove(tileDraws[i]);
		}
	}
}
