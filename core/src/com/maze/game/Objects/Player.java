package com.maze.game.Objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.*;
import com.maze.game.BaseObjects.AnimatedEntity;
import com.maze.game.BaseObjects.DrawLight;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Stages.FInalStage;
import com.maze.game.Stages.MazeStage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

/**
 * Created by zachm on 10/2/2017.
 */
public class Player extends AnimatedEntity implements DrawLight {
	Animation[] idle = new Animation[4];
	Animation[] idleBlink = new Animation[4];
	Animation[] walk = new Animation[4];
	Animation[] run = new Animation[4];
	Animation[] slide = new Animation[4];

	Sound[] footsteps = new Sound[3];
	float stepdist = 0.5f;
	float stepTime = 0;

	public boolean running = false;
	public boolean againstWall = false;
	public boolean invisible = false;
	public ArrayList<Float> inventory = new ArrayList<Float>();
	Vector2 wallDirection = new Vector2();
	int dir = 0;
	boolean godMode = false;

	float lightSize = 3f;
	Sprite lightSprite;
	Sprite blackout;

	MazeStage stage;

	Texture key;

	public Player(Vector2 pos, float angle, int z, GameObject parent) {
		super(pos, angle, z, parent);
		setSprite("Player.png");
		offset.y = -1;
		idle[0] = ResourceManager.getAnim("Player/Idle/IdleUp.png", 25, 1/60f);
		idle[1] = ResourceManager.getAnim("Player/Idle/IdleRight.png", 25, 1/60f);
		idle[2] = ResourceManager.getAnim("Player/Idle/IdleDown.png", 25, 1/60f);
		idle[3] = ResourceManager.getAnim("Player/Idle/IdleLeft.png", 25, 1/60f);

		idleBlink[0] = ResourceManager.getAnim("Player/Idle/BlinkUp.png", 101, 1/60f);
		idleBlink[1] = ResourceManager.getAnim("Player/Idle/BlinkRight.png", 101, 1/60f);
		idleBlink[2] = ResourceManager.getAnim("Player/Idle/BlinkDown.png", 101, 1/60f);
		idleBlink[3] = ResourceManager.getAnim("Player/Idle/BlinkLeft.png", 101, 1/60f);

		walk[0] = ResourceManager.getAnim("Player/Walk/WalkUp.png", 67, 1/60f);
		walk[1] = ResourceManager.getAnim("Player/Walk/WalkRight.png", 67, 1/60f);
		walk[2] = ResourceManager.getAnim("Player/Walk/WalkDown.png", 67, 1/60f);
		walk[3] = ResourceManager.getAnim("Player/Walk/WalkLeft.png", 67, 1/60f);


		run[0] = ResourceManager.getAnim("Player/Run/RunUp.png", 35, 1/60f);
		run[1] = ResourceManager.getAnim("Player/Run/RunRight.png", 35, 1/60f);
		run[2] = ResourceManager.getAnim("Player/Run/RunDown.png", 35, 1/60f);
		run[3] = ResourceManager.getAnim("Player/Run/RunLeft.png", 35, 1/60f);

		slide[0] = ResourceManager.getAnim("Player/Slide/SlideUp.png", 57, 1/60f);
		slide[1] = ResourceManager.getAnim("Player/Slide/SlideRight.png", 57, 1/60f);
		slide[2] = ResourceManager.getAnim("Player/Slide/SlideDown.png", 57, 1/60f);
		slide[3] = ResourceManager.getAnim("Player/Slide/SlideLeft.png", 57, 1/60f);

		footsteps[0] = ResourceManager.getSound("Sounds/Feets/Step01.mp3");
		footsteps[1] = ResourceManager.getSound("Sounds/Feets/Step02.mp3");
		footsteps[2] = ResourceManager.getSound("Sounds/Feets/Step03.mp3");

		lightSprite = new Sprite(ResourceManager.getImage("light.png"));
		blackout = new Sprite(ResourceManager.getImage("shadow.png"));

		key = ResourceManager.getImage("Key.png");

		setAnim(idle[0], 0, "idle");

		if (Game.currentStage instanceof MazeStage) {
			stage = (MazeStage) Game.currentStage;
		}
	}

	@Override
	public void update() {
		if (Compare.Vector(pos, stage.maze.endPoint, stage.maze.tileScaleWidth * 0.3f, stage.maze.tileScaleHeight * 0.3f) | (Game.debug && Gdx.input.isKeyJustPressed(Settings.cheat2))) {
			if (stage.currentLevel < 2) {
				Game.setStage(new MazeStage(stage.currentLevel + 1));
			} else if (stage.currentLevel < 3) {
				Game.setStage(new FInalStage(100));
			} else {
				stage.player = null;
				Delete();
			}
		}

		stage.maze.updateHidden(pos);

		int speed = 120;
		velocity.set(0, 0);
		if (Gdx.input.isKeyPressed(Settings.up)) {
			velocity.y += speed;
		}

		if (Gdx.input.isKeyPressed(Settings.down)) {
			velocity.y -= speed;
		}

		if (Gdx.input.isKeyPressed(Settings.left)) {
			velocity.x -= speed;
		}

		if (Gdx.input.isKeyPressed(Settings.right)) {
			velocity.x += speed;
		}

		velocity.nor().scl(speed);

		if (velocity.x > 0) {
			dir = 1;
		}

		if (velocity.x < 0) {
			dir = 3;
		}

		if (velocity.y > 0) {
			dir = 0;
		}

		if (velocity.y < 0) {
			dir = 2;
		}

		if (againstWall) {
			//System.out.print("Playr Is Against The Wall in Direction : " + wallDirection);
			if ((velocity.x != 0 && wallDirection.x == 0) | (velocity.y != 0 && wallDirection.y == 0)) {
				//System.out.print(" And Player Is Sliding");
				velScale.set(velocity).scl(Time.deltaTime);
				if (stage.maze.isTileOpen(new Vector2(pos).add(velScale))) {
					//System.out.print(" And Player should Stop sliding");
					if (wallDirection.x != 0) {
						//System.out.print(" Stop y");
						velocity.y = 0;
					}
					if (wallDirection.y != 0) {
						//System.out.print(" Stop x");
						velocity.x = 0;
					}
				}
			}
			//System.out.print("\n");
		}

		running = false;
		if (Gdx.input.isKeyPressed(Settings.sprint)) {
			if (!againstWall) {
				velocity.scl(4f);
				running = true;
			}
		}

		invisible = Gdx.input.isKeyPressed(Settings.invisible);

		int scroll = 0;
		if (InProcess.scroll!= 0 && ! (stage instanceof FInalStage)) {
			Game.mainCamera.zoom += InProcess.scroll * 0.1f;
			if (Game.mainCamera.zoom < 1) {
				Game.mainCamera.zoom = 1f;
			}
			if (Game.mainCamera.zoom > 8) {
				Game.mainCamera.zoom = 8;
			}
			//System.out.println(InProcess.scroll);
		}

		if (velocity.len2() != 0) {
			if (!running) {
				stepTime += Time.deltaTime;
			} else {
				stepTime += Time.deltaTime * 4;
			}
			if (stepTime >= stepdist) {
				if (!running) {
					footsteps[(int) (Game.random.nextFloat() * footsteps.length)].play(0.08f);
				} else {
					footsteps[(int) (Game.random.nextFloat() * footsteps.length)].play(0.25f);
				}
				stepTime = 0;
			}
		} else {
			stepTime = stepdist * 0.25f;
		}

		if (Game.debug && Gdx.input.isKeyJustPressed(Settings.godMode)) {
			godMode = ! godMode;
		}
		if (!Game.debug) {
			godMode = false;
		}
	}

	private void pushOut(Vector2 oldPos){
		int maxSteps = 100;
		//System.out.println(!stage.maze.isTileOpen(pos) + ", " + stage.maze.inMaze(pos));
		while(!stage.maze.isTileOpen(pos) && stage.maze.inMaze(pos) && maxSteps >= 0) {
			againstWall = true;
			maxSteps--;
			Vector2 newNode = stage.maze.posToTile(pos.x, pos.y);
			Vector2 oldNode = stage.maze.posToTile(oldPos.x, oldPos.y);
			Vector2 push = new Vector2((int) - newNode.x + (int) oldNode.x, (int) - newNode.y + (int) oldNode.y).nor();
			wallDirection.x = -push.x;
			wallDirection.y = -push.y;
			if (push.x == 0 && push.y == 0) {
				push.set(velScale).nor().scl(-1);
			}
			if (push.x == 0 && push.y == 0) {
				push.y = 1;
				push.x = 1;
			}
			push.scl(0.1f);
			//System.out.println(push);
			pos.add(push);
			localPos.add(push);
			//System.out.println(pos + " : " + localPos + "\n");
		}
	}


	@Override
	public void lateUpdate() {
		Vector2 old = new Vector2(localPos);

		super.lateUpdate();
		againstWall = false;
		if (stage != null) {
			Vector2 oldPos = new Vector2(localPos).sub(velScale);
			localPos.x = oldPos.x;
			localPos.y = oldPos.y;
			localPos.x += velScale.x;
			pos.set(localPos);
			pushOut(oldPos);
			localPos.y += velScale.y;
			oldPos.x += velScale.x;
			pos.set(localPos);
			pushOut(oldPos);

		}

		z = (int) (-pos.y);
		if (! (stage instanceof FInalStage)) {
			Game.mainCamera.position.x = pos.x;
			Game.mainCamera.position.y = pos.y;
		}

		if (againstWall) {
			if (wallDirection.x < 0) {
				dir = 1;
			}
			if (wallDirection.x > 0) {
				dir = 3;
			}
			if (wallDirection.y < 0) {
				dir = 0;
			}
			if (wallDirection.y > 0) {
				dir = 2;
			}
		}

		if (animState != null) {
			if ((int) old.x == (int) localPos.x && (int) old.y == (int) localPos.y) {
				running = false;
				if (animState.compareTo("idle") != 0) {
					setAnim(idle[dir], 0, "idle");
				}
				if (anim.isAnimationFinished(stateTime)) {
					float random = Game.random.nextFloat();
					//System.out.println(random);
					if (random > 0.95f) {
						setAnim(idleBlink[dir], 0);
					} else {
						setAnim(idle[dir], 0);
					}
				}
			} else {
				if (running) {
					setAnim(run[dir], stateTime, "run");
				} else if (againstWall) {
					setAnim(slide[dir], stateTime, "slide");
				} else if (!running && ! againstWall){
					setAnim(walk[dir], stateTime, "walk");
				}
			}
		}
	}

	@Override
	public void renderLight(Batch graphics) {
		//System.out.println(invisible);
		if (invisible) {
			blackout.setCenter(Game.mainCamera.position.x, Game.mainCamera.position.y);
			blackout.setScale(Game.GAME_WIDTH * Game.mainCamera.zoom, Game.GAME_HEIGHT * Game.mainCamera.zoom);
			blackout.draw(graphics);
		} else {
			if (godMode) {
				lightSprite.setColor(Color.GOLD);
			} else if (stage instanceof FInalStage) {
				lightSprite.setColor(new Color(1.0f, 1.0f, 1.0f, 1));
			}else{
				lightSprite.setColor(new Color(1.0f, 0.8f, 0.8f, 1));
			}
			lightSprite.setCenter(pos.x, pos.y);
			lightSprite.setRotation(rot);
			lightSprite.setScale(lightSize);
			lightSprite.draw(graphics);
		}
	}

	@Override
	public int lightz() {
		return 0;
	}

	@Override
	public void renderGUI(Batch batch) {
		if (inventory.size() > 0) {
			Collections.sort(inventory);
			float x = -Game.GAME_WIDTH / 2 * 5 + 32;
			float y = -Game.GAME_HEIGHT / 2 * 5;
			batch.draw(key, x - key.getWidth() / 2, y);
			String drawStr = "" + inventory.get(0).intValue();
			for (int i = 1; i < inventory.size(); i++) {
				drawStr += ", " + inventory.get(i).intValue();
			}
			Game.fonts[0].draw(batch, drawStr, x + 16, y + 32);
		}
	}

	@Override
	public int guiz() {
		return 100;
	}

	public void kill() {
		if (stage != null && !godMode) {
			Delete();
			stage.player = null;
		}
	}
}
