package com.maze.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.Tile;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.ResourceManager;

/**
 * Created by zachm on 10/24/2017.
 */
public class FinalWorld extends Maze {
	public FinalWorld() {
		super(5, 5, -1, -1, -1, -1);
	}

	@Override
	public void generateMaze() {
		floor = "blank.png";
		wall = "Wall/Fence/Fence.png";
		wallBase = "Wall/Fence/Fence";
		destroyMaze();
		nodes = new MazeNode[width][height];
		tileArray = new Tile[width * 2 + 1][height * 2 + 1];
		endpoints.clear();
		gates.clear();
		keys.clear();

		for (GameObject o : extraObjects) {
			o.Delete();
		}
		extraObjects.clear();

		tileOpen = new boolean[width*2 + 1][height*2 + 1];
		for (int x = 0; x < tileOpen.length; x++) {
			for (int y = 0; y < tileOpen[0].length; y++) {
				if (y == height) {
					Game.addGameObject(new Tile("path.png", x * tileScaleWidth, (y - 1) * tileScaleHeight, -(y - 1) * tileScaleHeight));
				}
				tileOpen[x][y] = true;
				if (x == 0 | y == 0 | x >= width * 2 | y >= width * 2) {
					tileOpen[x][y] = false;
				}
				Tile t = new Tile(floor, (x - 1) * tileScaleWidth, (y - 1) * tileScaleHeight, -((y - 1) * tileScaleHeight) - tileScaleHeight);
				if (!tileOpen[x][y]) {
					t.sprite.setTexture(ResourceManager.getImage(wall));
					t.sprite.setSize(t.sprite.getTexture().getWidth(), t.sprite.getTexture().getHeight());
					t.z = (int) -t.pos.y;
				}
				tiles.add(t);
				Game.drawObjects.add(t);
				tileArray[x][y] = t;
			}
		}

		startPoint = new Vector2(tileToPos(new Vector2(1, height)));
		endPoint = new Vector2(tileToPos(new Vector2(width * 2, height)));
		breakTile(new Vector2(width * 2, height), false);
		//Game.addGameObject(new Tile("Enterance.png", endPoint.x, endPoint.y, (int) -endPoint.y - tileScaleHeight/2));
	}

	@Override
	public void updateHidden(Vector2 pos) {
	}
}
