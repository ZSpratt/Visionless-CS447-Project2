package com.maze.game.Objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.Entity;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.StringDraw;
import com.maze.game.Compare;
import com.maze.game.Game;
import com.maze.game.Stages.MazeStage;


public class Gate extends Entity {
	int keyPair;
	Vector2 tilePos;
	MazeStage stage;
	StringDraw sd;
	public Gate(Vector2 pos, int value) {
		super(pos, 0, (int) -pos.y, null);
		setSprite("Gate.png");
		keyPair = value;

		stage = (MazeStage) Game.currentStage;
		tilePos = stage.maze.posToTile(pos.x, pos.y);
		sd = new StringDraw(keyPair + "", pos.x, pos.y + 4, 0, 4);
	}

	@Override
	public void update() {
		sd.update();
		if (!delete) {
			stage.maze.tileOpen[(int) tilePos.x][(int) tilePos.y] = false;
		}

		if (stage.player != null) {
			if (Compare.Vector(pos, stage.player.pos, stage.maze.nodeScaleWidth / 2 + 12, stage.maze.tileScaleHeight / 2 + 8)) {
				if (stage.player.inventory.contains(new Float (keyPair))) {
					stage.player.inventory.remove(new Float(keyPair));
					Delete();
					stage.maze.breakTile(pos, true);
					stage.maze.tileOpen[(int) tilePos.x][(int) tilePos.y] = true;
				}
			}
		}
	}

	@Override
	public void render(Batch batch) {
		super.render(batch);
		sd.renderGUI(batch);
	}
}
