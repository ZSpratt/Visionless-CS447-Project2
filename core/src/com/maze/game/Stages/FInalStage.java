package com.maze.game.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.Stage;
import com.maze.game.BaseObjects.Tile;
import com.maze.game.Game;
import com.maze.game.Objects.*;
import com.maze.game.ResourceManager;

/**
 * Created by zachm on 10/24/2017.
 */
public class FInalStage extends MazeStage {

	public FInalStage(int level) {
		super(level);
	}

	@Override
	public void onStartStage() {

		Game.backGroundPosSize = new float[4];
		Game.backGroundPosSize[0] = -1000;
		Game.backGroundPosSize[1] = -1000;
		Game.backGroundPosSize[2] = 2000 + 1000 * 52 * 2;
		Game.backGroundPosSize[3] = 2000 + 1000 * 52 * 2;

		Game.background = ResourceManager.getImage("Grass.png");
		Game.background.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
		Game.backgroundColor = new Color(0.9f, 0.9f, 0.9f, 1.0f);

		maze = new FinalWorld();
		maze.generateMaze();
		player = (Player) Game.addGameObject(new Player(maze.startPoint, 0, 0, null));
		map = (MapView) Game.addGameObject(new MapView());

		Vector2 camPos = maze.tileToPos(new Vector2(maze.width, maze.height));
		Game.mainCamera.position.set(camPos.x, camPos.y, 0);
		Game.mainCamera.zoom = 4.5f;

		Game.addGameObject(new GameplayTracker());
		Game.addGameObject(new Tile("exterior.png", maze.startPoint.x - 54, maze.startPoint.y, 10000));
	}

	@Override
	public void update() {
		super.update();
		if (player != null) {
			Game.backgroundColor = new Color(0.75f, 0.75f, 1f, 1.0f);
			if (!maze.inMaze(player.pos)) {
				//player.localPos.set(maze.startPoint);
			}
		} else {
			Game.backgroundColor = new Color(0f, 0f, 0f, 1);
		}

		maze.updateTileayout();
	}

	@Override
	public void lateUpdate() {
		super.lateUpdate();
	}
}
