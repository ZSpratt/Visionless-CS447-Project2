package com.maze.game.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.maze.game.BaseObjects.*;
import com.maze.game.Game;
import com.maze.game.Objects.ControllsWindow;
import com.maze.game.Objects.GameplayTracker;
import com.maze.game.Objects.StatsWindow;
import com.maze.game.ResourceManager;
import com.maze.game.Settings;
import com.maze.game.Time;

import java.util.ArrayList;

/**
 * Created by zachm on 10/16/2017.
 */
public class StartMenu extends Stage {

	float tmpx = 0;
	float tmpy = 0;
	boolean tracking = false;
	GUIBox currentBox;

	int selected = 0;
	int options = 5;

	StringDraw[] selectStrings = new StringDraw[options];
	StringDraw debugString;
	StatsWindow stats = null;
	ControllsWindow conts = null;

	ArrayList<GUITile> stars = new ArrayList<GUITile>();
	Color[] colors = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE};
	public float starColorTimer = 0;
	private Tile playerDisplay;

	@Override
	public void onStartStage() {
		Time.timeScale = 1;
		Game.loadGame();
		Game.mainCamera.zoom = 1;
		Game.mainCamera.position.set(0, 0, 0);
		Game.backgroundColor = Color.WHITE;
		Game.background = null;
		Game.mainCamera.zoom = 5;

		float menuX = -250;
		float menuY = 0;
		Game.addGameObject(new GUIBox(menuX, menuY, 200, 400, 0));

		Game.addGameObject(new StringDraw("Visionless", menuX, menuY + 175, Color.WHITE, Game.fonts[2], 10, 1));
		selectStrings[0] = (StringDraw) Game.addGameObject(new StringDraw("Start", menuX - 100, menuY + 50, Color.WHITE, Game.fonts[1], 10, 3));
		selectStrings[1] = (StringDraw) Game.addGameObject(new StringDraw("Stats", menuX - 100, menuY - 0, Color.WHITE, Game.fonts[1], 10, 3));
		selectStrings[2] = (StringDraw) Game.addGameObject(new StringDraw("Controls", menuX - 100, menuY - 50, Color.WHITE, Game.fonts[1], 10, 3));
		debugString = selectStrings[3] = (StringDraw) Game.addGameObject(new StringDraw("Debug : F", menuX - 100, menuY - 100, Color.WHITE, Game.fonts[1], 10, 3));
		selectStrings[4] = (StringDraw) Game.addGameObject(new StringDraw("Exit", menuX - 100, menuY - 150, Color.WHITE, Game.fonts[1], 10, 3));
		int space = 38;
		if (GameplayTracker.deathChallange) {
			stars.add((GUITile) Game.addGameObject(new GUITile("Achives/Star.png", menuX - space * 2, menuY + 110, 100)));
		}
		if (GameplayTracker.tilesChallange) {
			stars.add((GUITile) Game.addGameObject(new GUITile("Achives/Star.png", menuX - space, menuY + 110, 100)));
		}
		if (GameplayTracker.exploreChallange) {
			stars.add((GUITile) Game.addGameObject(new GUITile("Achives/Star.png", menuX, menuY + 110, 100)));
		}
		if (GameplayTracker.floorChallange) {
			stars.add((GUITile) Game.addGameObject(new GUITile("Achives/Star.png", menuX + space, menuY + 110, 100)));
		}
		if (GameplayTracker.gameChallange) {
			stars.add((GUITile) Game.addGameObject(new GUITile("Achives/Star.png", menuX + space * 2, menuY + 110, 100)));
		}

		playerDisplay = (Tile) Game.addGameObject(new Tile("Achives/Player.png", 250, 0, 0));
		playerDisplay.scale = 0.25f;
		if (GameplayTracker.numChallange >= 3) {
			playerDisplay.sprite.setTexture(ResourceManager.getImage("Achives/PlayerRev.png"));
		}
		if (GameplayTracker.challangesCompleted) {
			playerDisplay.sprite.setTexture(ResourceManager.getImage("Achives/PlayerFin.png"));
		}
		playerDisplay.sprite.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		playerDisplay.color = new Color(1, 1, 1, 0.35f);


	}

	private Color starColor(int offset) {
		starColorTimer += Time.deltaTime * 0.125f;
		starColorTimer %= stars.size();
		int i = (int) starColorTimer;
		float d = starColorTimer - i;
		i = (i + offset) % stars.size();

		Color x = colors[i];
		Color y = colors[0];
		if (i < stars.size() - 1) {
			y = colors[i + 1];
		}
		float r = (1 - d) * (x.r * x.r) + d * (y.r * y.r);
		float g = (1 - d) * (x.g * x.g) + d * (y.g * y.g);
		float b = (1 - d) * (x.b * x.b) + d * (y.b * y.b);
		return new Color(r, g, b, 1.0f);
	}

	@Override
	public void update() {
		/*if (playerDisplay.pos.x <= 250) {
			playerDisplay.pos.x += 5 * Time.deltaTime;
			playerDisplay.pos.y -= 2.5f * Time.deltaTime;
			playerDisplay.pos.x = 250;
		}*/
		for (int i = 0; i < stars.size(); i++) {
			stars.get(i).color = starColor(i);
		}

		if (Game.debug == true) {
			debugString.str = "Debug : T";
		} else {
			debugString.str = "Debug : F";
		}

		selectStrings[selected].color = Color.WHITE;
		if (Gdx.input.isKeyJustPressed(Settings.up)) {
			selected -= 1;
		}

		if (Gdx.input.isKeyJustPressed(Settings.down)) {
			selected += 1;
		}

		selected %= options;
		if (selected < 0) {
			selected += options;
		}
		selectStrings[selected].color = Color.SKY;



		if (Gdx.input.isKeyJustPressed(Settings.invisible)) {
			switch (selected) {
				case 0:
					Game.setStage(new MazeStage(0));
					break;
				case 1:
					if (conts != null) {
						conts.Delete();
						conts = null;
					}
					if (stats == null) {
						stats = (StatsWindow) Game.addGameObject(new StatsWindow());
					} else {
						stats.Delete();
						stats = null;
					}
					break;
				case 2:
					if (stats != null) {
						stats.Delete();
						stats = null;
					}
					if (conts == null) {
						conts = (ControllsWindow) Game.addGameObject(new ControllsWindow());
					} else {
						conts.Delete();
						conts = null;
					}
					break;
				case 3:
					Game.debug = !Game.debug;
					break;
				case 4:
					Gdx.app.exit();
					break;
			}
		}

		if (Game.debug) {
			if (Gdx.input.isKeyJustPressed(Settings.cheat1)) {
				Game.setStage(new FInalStage(100));
			}
			if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
				if (!tracking) {
					tmpx = Game.mouseX;
					tmpy = Game.mouseY;
					tracking = true;
					currentBox = (GUIBox) Game.addGameObject(new GUIBox(tmpx, tmpy, 0, 0, -1));
				} else {
					float width = Math.abs(Game.mouseX - tmpx) * 2;
					float height = Math.abs(Game.mouseY - tmpy) * 2;
					currentBox.width = width;
					currentBox.height = height;
				}
			} else {
				tracking = false;
			}
		}
	}
}
