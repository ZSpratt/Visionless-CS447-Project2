package com.maze.game.Stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.BaseObjects.GameObject;
import com.maze.game.BaseObjects.Stage;
import com.maze.game.Game;
import com.maze.game.Objects.*;
import com.maze.game.ResourceManager;
import com.maze.game.Settings;
import com.maze.game.Time;

import java.util.Set;

/**
 * Created by zachm on 10/16/2017.
 */
public class MazeStage extends Stage {
	public Player player;
	public Maze maze;

	public int currentLevel;
	private int mazeSize = 5;

	GameObject pauseMenu;
	boolean gameOver = false;

	public MapView map;
	float breakChance = 0.125f;
	Sound bgm = ResourceManager.getSound("Sounds/Visionless Background.wav");

	public MazeStage(int level) {
		currentLevel = level;
		if (level == 0) {
			mazeSize = 5;
		} else if (level == 1) {
			mazeSize = 10;
		}else {
			this.mazeSize = 30;
		}
	}

	@Override
	public void onStartStage() {
		Time.timeScale = 1;

		Game.backGroundPosSize = new float[4];
		Game.backGroundPosSize[0] = -1000;
		Game.backGroundPosSize[1] = -1000;
		Game.backGroundPosSize[2] = 2000 + 1000 * 52 * 2;
		Game.backGroundPosSize[3] = 2000 + 1000 * 52 * 2;

		Game.background = ResourceManager.getImage("background.png");
		Game.background.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

		maze = new Maze(mazeSize, mazeSize, 0.05f, 0.15f, 0.15f, breakChance);
		maze.generateMaze();
		/*
		for (int i = 0; i < 10; i++) {
			if (maze.nonEndPoints.size() >= 1) {
				Vector2 p = maze.nonEndPoints.get((int) (Game.random.nextFloat() * maze.nonEndPoints.size()));
				maze.nonEndPoints.remove(p);
				//System.out.println(maze.posToTile(p.x, p.y));
				Game.addGameObject(new PitfallTrap(p, 0, 0, null));
			}
		}

		for (int i = 0; i < 10; i++) {
			if (maze.nonEndPoints.size() >= 1) {
				Vector2 p = maze.nonEndPoints.get((int) (Game.random.nextFloat() * maze.nonEndPoints.size()));
				maze.nonEndPoints.remove(p);
				//System.out.println(maze.posToTile(p.x, p.y));
				Game.addGameObject(new WallSpikes(p, 0, 0, null));
			}
		}
		*/
		player = (Player) Game.addGameObject(new Player(maze.startPoint, 0, 0, null));

		if (currentLevel > 0) {
			int golemCount = 1;
			if (currentLevel > 1) {
				golemCount = 10;
			}
			for (int i = 0; i < golemCount; i++) {
				if (maze.endpoints.size() > 1) {
					Vector2 pos = new Vector2(maze.endpoints.get((int) (Game.random.nextFloat() * maze.endpoints.size())));
					maze.endpoints.remove(pos);
					if (!maze.rayCast(pos, maze.startPoint)) {
						Game.addGameObject(new Golem(pos, 0, 0, null));
					}
				}
			}
		}

		map = (MapView) Game.addGameObject(new MapView());
		Game.addGameObject(new GameplayTracker());
	}

	@Override
	public void update() {
		//maze.updateTileayout();
		map.visible = false;

		if (Gdx.input.isKeyJustPressed(Settings.cheat1) && Game.debug) {
			Game.addGameObject(new Golem(new Vector2(Game.mouseX, Game.mouseY), 0, 0, null));
		}

		if (player != null) {
			Game.backgroundColor = new Color(0.01f, 0.01f, 0.11f, 1);
		} else {
			Game.backgroundColor = new Color(0f, 0f, 0f, 1);
		}

		if (player == null) {
			if (! gameOver) {
				pauseMenu = Game.addGameObject(new PauseMenu(false));
				gameOver = true;
			}
		} else {
			map.visible = Gdx.input.isKeyPressed(Settings.map) && pauseMenu == null;
			if (Game.debug) {
				if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
					maze.breakTile(new Vector2(Game.mouseX, Game.mouseY), true);
				}

				if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
					maze.buildTile(new Vector2(Game.mouseX, Game.mouseY), true);
				}

				/*if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
					maze.generateMaze();
					maze.randomBreak(breakChance);
				}*/
			}

			if (Gdx.input.isKeyJustPressed(Settings.pause) | (pauseMenu != null && pauseMenu.delete)) {
				if (pauseMenu == null) {
					pauseMenu = Game.addGameObject(new PauseMenu(true));
					Time.timeScale = 0;
				} else {
					pauseMenu.Delete();
					pauseMenu = null;
					Time.timeScale = 1;
				}
			}
		}
	}
}
