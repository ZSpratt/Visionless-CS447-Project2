package com.maze.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Hashtable;

public class ResourceManager {
    private static Hashtable<String, Texture> images = new Hashtable<String, Texture>();
    private static Hashtable<String, Sound> sounds = new Hashtable<String, Sound>();

    public static String root = "src/BreakInvaders/resource/";

	public static void loadImage(String dir) {
		try {
			Texture i = new Texture(dir);
			i.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
			images.put(dir, i);
		} catch (Exception e) {
			System.err.println("Texture \"" + dir + "\" Not Loaded");
		}
	}

	public static void loadSound(String dir) {
		try {
			Sound i = Gdx.audio.newSound(Gdx.files.internal(dir));
			sounds.put(dir, i);
		} catch (Exception e) {
			System.err.println("Sound \"" + dir + "\" Not Loaded");
		}
	}

    public static Texture getImage(String dir) {
        if (!images.containsKey(dir)) {
            loadImage(dir);
        }
        return images.get(dir);
    }

    public static Sound getSound(String dir) {
        if (!sounds.containsKey(dir)) {
            loadSound(dir);
        }
        return sounds.get(dir);
    }

	public static Animation<TextureRegion> getAnim(String dir, int frameCount, float frameDuration) {
		int splitW = (int) Math.sqrt(frameCount);
		int splitH = splitW;
		while (splitW * splitH < frameCount){
			splitH += 1;
		}

		Texture idle = ResourceManager.getImage(dir);
		TextureRegion[][] tmp = TextureRegion.split(idle, idle.getWidth()/splitW, idle.getHeight()/splitH);
		TextureRegion[] frames = new TextureRegion[frameCount];

		int diff = splitH * splitW - frameCount;
		//System.out.println(splitH + ", " + splitW + " : " + frameCount + " -> " + diff);
		//System.out.println(tmp.length + ", " + tmp[0].length);

		int index = -1;
		for (int i = 0; i < splitH; i++) {
			for (int j = 0; j < splitW; j++) {
				index++;
				//System.out.println(index + " : " + tmp[i][j]);
				if (index < frameCount) {
					frames[index] = tmp[i][j];
				} else {
					break;
				}
			}
		}
		Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, frames);
		anim.setPlayMode(Animation.PlayMode.LOOP);
		return anim;
	}
}
