package com.maze.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.maze.game.Game;
import com.maze.game.ResourceManager;
import com.maze.game.Time;

public class Entity extends GameObject implements Drawable, DrawableGUI {
    public Sprite sprite;
    public Shape2D collider;
    public Color color;

    public Vector2 velScale = new Vector2(0, 0);
    public Vector2 velocity = new Vector2();
    public boolean kinematic = false;

    public float size;

    public int collisionLayer = -1;

    public Entity(Vector2 pos, float angle, int z, GameObject parent) {
        super(pos, angle, z, parent);
        color = Color.WHITE;
    }

    public void setSprite(String res) {
        sprite = new Sprite(ResourceManager.getImage(res));
        float xoff = sprite.getWidth() / 2;
        float yoff = sprite.getHeight() / 2;
        collider = new Rectangle(-xoff, -yoff, xoff * 2, yoff * 2);
        size = sprite.getWidth() * sprite.getWidth() + sprite.getHeight() * sprite.getHeight();
        size = (float) Math.sqrt(size);
        size /= 2f;
    }

    @Override
    public void render(Batch batch) {
        if (pos.x + offset.x + sprite.getWidth()/2 * Game.mainCamera.zoom > Game.mainCamera.position.x - Game.GAME_WIDTH/2 * Game.mainCamera.zoom) {
            if (pos.x + offset.x - sprite.getWidth()/2 * Game.mainCamera.zoom < Game.mainCamera.position.x + Game.GAME_WIDTH / 2 * Game.mainCamera.zoom) {
                if (pos.y + offset.y + sprite.getHeight()/2 * Game.mainCamera.zoom > Game.mainCamera.position.y - Game.GAME_HEIGHT/2 * Game.mainCamera.zoom) {
                    if (pos.y  + offset.y - sprite.getHeight()/2 * Game.mainCamera.zoom < Game.mainCamera.position.y + Game.GAME_HEIGHT / 2 * Game.mainCamera.zoom) {
                        //graphics.draw(getCollider());
                        sprite.setRotation(rot);
                        sprite.setCenter(pos.x + offset.x, pos.y + offset.y);
                        sprite.setColor(color);
                        sprite.draw(batch);
                    }
                }
            }
        }

    }

    @Override
    public int z() {
        return z;
    }

    @Override
    public void renderGUI(Batch batch) {
        return;
    }

    @Override
    public int guiz() {
        return z;
    }

    @Override
    public void lateUpdate() {
        if (velocity != null && velScale != null) {
            velScale.set(velocity);
            velScale.scl(Time.deltaTime);
            localPos.add(velScale);
        }
        super.lateUpdate();
    }
}