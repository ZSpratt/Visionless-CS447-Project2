package com.maze.game.BaseObjects;

import com.maze.game.BaseObjects.GameObject;
import com.maze.game.Game;
import com.maze.game.ResourceManager;

public class Stage extends GameObject {
    public String[] preloadSprites;
    public String[] preloadSounds;
    public boolean stageOver = false;

    public Stage() {
        super(0, 0, 0, 0);
    }

    public void startStage() {
        Game.clearAllObjects();
        Game.addGameObject(this);

        if (preloadSprites != null) {
            for (int i = 0; i < preloadSprites.length; i++) {
                ResourceManager.loadImage(preloadSprites[i]);
            }
        }

        if (preloadSounds != null) {
            for (int i = 0; i < preloadSounds.length; i++) {
                ResourceManager.loadSound(preloadSounds[i]);
            }
        }

        onStartStage();
    }

    public void onStartStage() {

    }


    public void endStage() {
        stageOver = true;
        onEndStage();
    }

    public void onEndStage() {

    }
}
