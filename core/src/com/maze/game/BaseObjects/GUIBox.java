package com.maze.game.BaseObjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.maze.game.ResourceManager;

/**
 * Created by zachm on 10/17/2017.
 */
public class GUIBox extends GameObject implements DrawableGUI {

	TextureRegion[][] parts;
	public float width, height;

	int cornerHeight, cornerWidth;
	public GUIBox(float x, float y, float width, float height, int z) {
		super(x, y, 0, z);
		Texture box = ResourceManager.getImage("GUI/9Square.png");
		cornerWidth = 11;
		cornerHeight = 11;
		int remWidth = box.getWidth() - 2 * cornerWidth;
		int remHeight = box.getHeight() - 2 * cornerHeight;
		parts = new TextureRegion[3][3];

		this.width = width;
		this.height = height;

		parts[0][0] = new TextureRegion(box, 0, 0, cornerWidth, cornerHeight);
		parts[1][0] = new TextureRegion(box, cornerWidth, 0, remWidth, cornerHeight);
		parts[2][0] = new TextureRegion(box, cornerWidth + remWidth, 0, cornerWidth, cornerHeight);

		parts[0][1] = new TextureRegion(box, 0, cornerHeight, cornerWidth, remHeight);
		parts[1][1] = new TextureRegion(box, cornerWidth, cornerHeight, remWidth, remHeight);
		parts[2][1] = new TextureRegion(box, cornerWidth + remWidth, cornerHeight, cornerWidth, remHeight);

		parts[0][2] = new TextureRegion(box, 0, cornerHeight + remHeight, cornerWidth, cornerHeight);
		parts[1][2] = new TextureRegion(box, cornerWidth, cornerHeight + remHeight, remWidth, cornerHeight);
		parts[2][2] = new TextureRegion(box, cornerWidth + remWidth, cornerHeight + remHeight, cornerWidth, cornerHeight);

	}

	@Override
	public void update() {
	}

	@Override
	public void renderGUI(Batch batch) {
		float xOff = -width/2;
		float yOff = -height/2;
		batch.setColor(Color.WHITE);

		batch.draw(parts[0][0], localPos.x + xOff - cornerWidth, localPos.y + yOff + height, cornerWidth, cornerHeight);
		batch.draw(parts[1][0], localPos.x + xOff, localPos.y + yOff + height, width, cornerHeight);
		batch.draw(parts[2][0], localPos.x + xOff + width, localPos.y + yOff + height, cornerWidth, cornerHeight);

		batch.draw(parts[0][1], localPos.x + xOff - cornerWidth, localPos.y + yOff, cornerWidth, height);
		batch.draw(parts[1][1], localPos.x + xOff, localPos.y + yOff, width, height);
		batch.draw(parts[2][1], localPos.x + xOff + width, localPos.y + yOff, cornerWidth, height);

		batch.draw(parts[0][2], localPos.x + xOff - cornerWidth, localPos.y + yOff - cornerHeight, cornerWidth, cornerHeight);
		batch.draw(parts[1][2], localPos.x + xOff, localPos.y + yOff - cornerHeight, width, cornerHeight);
		batch.draw(parts[2][2], localPos.x + xOff + width, localPos.y + yOff - cornerHeight, cornerWidth, cornerHeight);
	}

	@Override
	public int guiz() {
		return z;
	}
}
