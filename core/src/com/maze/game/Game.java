package com.maze.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.maze.game.BaseObjects.*;
import com.maze.game.Objects.GameplayTracker;
import com.maze.game.Objects.Golem;
import com.maze.game.Objects.Maze;
import com.maze.game.Objects.Player;
import com.maze.game.Stages.MazeStage;
import com.maze.game.Stages.StartMenu;

import java.util.*;
import java.io.*;

public class Game extends ApplicationAdapter {
	public static int GAME_WIDTH = 150;
	public static int GAME_HEIGHT = 100;

	public static boolean debug = false;
	public static boolean profile = false;
	private Hashtable<String, classProfile> profiles = new Hashtable<String, classProfile>();
	private float profileInterval = 1;
	private float currentProfile = 0;

	public static SpriteBatch spriteBatch;
	public static SpriteBatch renderBatch;
	public static ShapeRenderer shapeBatch;
	FrameBuffer fb;

	public static OrthographicCamera mainCamera;
	public static OrthographicCamera renderCam;
	public static Viewport viewport;

	public static Stage currentStage;
	public static Stage nextStage;
	public static boolean swapStage = false;

	private static ArrayList<GameObject> objects = new ArrayList<GameObject>();
	private static ArrayList<GameObject> deleteObjects = new ArrayList<GameObject>();
	public static ArrayList<Drawable> drawObjects = new ArrayList<Drawable>();
	private static ArrayList<DrawLight> drawLights = new ArrayList<DrawLight>();
	private static ArrayList<DrawableGUI> drawObjectsGUI = new ArrayList<DrawableGUI>();

	private Comparator<Drawable> drawComp = new Comparator<Drawable>() {
		@Override
		public int compare(Drawable o1, Drawable o2) {
			return  o1.z() - o2.z();
		}
	};
	private Comparator<DrawableGUI> drawGUIComp = new Comparator<DrawableGUI>() {
		@Override
		public int compare(DrawableGUI o1, DrawableGUI o2) {
			return o1.guiz() - o2.guiz();
		}
	};
	private Comparator<DrawLight> drawLightComp = new Comparator<DrawLight>() {
		@Override
		public int compare(DrawLight o1, DrawLight o2) {
			return o1.lightz() - o2.lightz();
		}
	};

	public static float mouseX;
	public static float mouseY;

	public static Random random;

	public static BitmapFont[] fonts = new BitmapFont[4];

	public static Texture background;
	public static float[] backGroundPosSize = {0, 0, 0, 0};
	private static ArrayList<Float[]> debugDrawLines = new ArrayList<Float[]>();

	public static Color backgroundColor = Color.BLACK;

	public static void addDebugLine(float x1, float y1, float x2, float y2 ) {
		Float[] line = new Float[4];
		line[0] = x1;
		line[1] = y1;
		line[2] = x2;
		line[3] = y2;
		debugDrawLines.add(line);
	}

	public static void clearDebugLide() {
		debugDrawLines.clear();
	}

	public static GameObject addGameObject(GameObject go) {
		objects.add(go);

		if (go instanceof Drawable) {
			drawObjects.add((Drawable) go);
		}

		if (go instanceof DrawableGUI) {
			drawObjectsGUI.add((DrawableGUI) go);
		}

		if (go instanceof DrawLight) {
			drawLights.add((DrawLight) go);
		}
		return  go;
	}

	public static void setStage(Stage newStage) {
		swapStage = true;
		if (currentStage != null) {
			currentStage.endStage();
		}
		nextStage = newStage;
	}

	public static void clearAllObjects() {
		for (int i = 0; i < objects.size(); i++) {
			objects.get(i).Delete();
		}
		objects.clear();
		deleteObjects.clear();
		drawObjects.clear();
		drawObjectsGUI.clear();
		drawLights.clear();
	}

	public static void saveGame() {
		try {
			File f = new File("Save.dat");
			f.createNewFile();

			PrintWriter save = new PrintWriter(f);

			save.write(GameplayTracker.deaths + "\n");
			save.write(GameplayTracker.tilesExplored + "\n");
			save.write(GameplayTracker.floorsFullExplored + "\n");
			save.write(GameplayTracker.floorsCleared + "\n");
			save.write(GameplayTracker.gamesWon + "\n");

			save.flush();
			save.close();
		} catch (IOException e) {
			System.err.println("Could not save");
			e.printStackTrace();
		}
	}

	public static void loadGame() {
		try {
			File f = new File("Save.dat");
			if (f.exists()) {
				FileReader file = new FileReader("Save.dat");
				BufferedReader load = new BufferedReader(file);
				String in;
				int i = 0;
				while ((in = load.readLine()) != null) {
					switch (i){
						case 0:
							GameplayTracker.deaths =  Integer.parseInt(in);
							break;
						case 1:
							GameplayTracker.tilesExplored =  Integer.parseInt(in);
							break;
						case 2:
							GameplayTracker.floorsFullExplored =  Integer.parseInt(in);
							break;
						case 3:
							GameplayTracker.floorsCleared =  Integer.parseInt(in);
							break;
						case 4:
							GameplayTracker.gamesWon =  Integer.parseInt(in);
							break;
					}
					i++;
				}
				load.close();
				GameplayTracker g = new GameplayTracker();
				g.Delete();
			}
		} catch (FileNotFoundException e) {
			System.err.println("Could not load");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Could not read");
			e.printStackTrace();
		}
	}

	@Override
	public void create () {
		ResourceManager.getSound("Sounds/Visionless Background.wav").loop(0.025f);
		random = new Random();
		spriteBatch = new SpriteBatch();
		renderBatch = new SpriteBatch();
		shapeBatch = new ShapeRenderer();
		fb = new FrameBuffer(Pixmap.Format.RGBA8888, GAME_WIDTH, GAME_HEIGHT, true);

		mainCamera = new OrthographicCamera(600, 400);
		renderCam = new OrthographicCamera(600, 400);
		viewport  = new FitViewport(GAME_WIDTH, GAME_HEIGHT, mainCamera);

		for (int i = 0; i < 4; i++) {
			fonts[i] = new BitmapFont(Gdx.files.internal("Fonts/Font[" + i + "].fnt"), Gdx.files.internal("Fonts/Font[" + i + "].png"), false);
		}

		InProcess in = new InProcess();
		Gdx.input.setInputProcessor(in);
		mainCamera.zoom = 2;

		currentStage = new StartMenu();
		currentStage.startStage();
	}

	@Override
	public void resize(int width, int height) {
		fb = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, true);
		viewport.update(width, height);
	}

	public void update() {
		clearDebugLide();
		Time.updateTime(Gdx.graphics.getDeltaTime());
		if (Gdx.input.isKeyJustPressed(Settings.profile))  {
			profile = !profile;
		}
		if (!profile) {
			currentProfile = 0;
		}

		if (currentStage != null && currentStage.stageOver) {
			clearAllObjects();
			currentStage = nextStage;
			currentStage.startStage();
			swapStage = false;
			profiles.clear();
		}

		for (int i = 0; i < objects.size(); i++) {
			long currentTime = System.nanoTime();
			objects.get(i).earlyUpdate();
			objects.get(i).update();

			if(objects.get(i).delete) {
				deleteObjects.add(objects.get(i));
			}

			objects.get(i).lateUpdate();
			if (profile && currentProfile == 0) {
				long deltaTime = System.nanoTime() - currentTime;
				String clas = objects.get(i).getClass().getName();
				if (profiles.containsKey(clas)) {
					profiles.get(clas).totalTime += deltaTime;
					profiles.get(clas).count += 1;
				} else {
					classProfile cp = new classProfile();
					cp.count = 1;
					cp.name = clas;
					cp.totalTime = deltaTime;
					profiles.put(clas, cp);
				}
			}
		}

		for (GameObject o : deleteObjects) {
			objects.remove(o);
			if (o instanceof  Drawable) {
				drawObjects.remove(o);
			}
			if (o instanceof DrawLight) {
				drawLights.remove(o);
			}
			if (o instanceof DrawableGUI) {
				drawObjectsGUI.remove(o);
			}
		}
		deleteObjects.clear();

		InProcess.scroll = 0;

		//System.out.println(player.z);
	}

	@Override
	public void render () {
		//System.out.println(Gdx.input.getX() + ", " + Gdx.input.getY());
		Vector3 v = viewport.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		mouseX =  v.x;
		mouseY =  v.y;

		//System.out.println(mouseX + ", " + mouseY);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update();

		mainCamera.update();
		spriteBatch.setProjectionMatrix(viewport.getCamera().combined);
		shapeBatch.setProjectionMatrix(viewport.getCamera().combined);
		spriteBatch.setBlendFunction(Gdx.gl.GL_SRC_ALPHA, Gdx.gl.GL_ONE_MINUS_SRC_ALPHA);

		viewport.apply();

		fb.begin();
		shapeBatch.begin(ShapeRenderer.ShapeType.Filled);
		shapeBatch.setColor(Color.BLACK);
		shapeBatch.rect(-viewport.getWorldWidth()/2 * mainCamera.zoom + mainCamera.position.x, -viewport.getWorldHeight()/2 * mainCamera.zoom + mainCamera.position.y, viewport.getWorldWidth() * mainCamera.zoom, viewport.getWorldHeight() * mainCamera.zoom);
		shapeBatch.end();

		spriteBatch.setColor(Color.WHITE);
		spriteBatch.begin();
		if (background != null) {
			if (backGroundPosSize != null) {
				spriteBatch.draw(background, backGroundPosSize[0], backGroundPosSize[1], 0, 0, (int) backGroundPosSize[2], (int) backGroundPosSize[3]);
			}
		}
		Collections.sort(drawObjects, drawComp);
		for (Drawable d : drawObjects) {
			d.render(spriteBatch);
		}

		spriteBatch.end();
		fb.end(viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

		spriteBatch.begin();

		spriteBatch.setColor(backgroundColor);

		spriteBatch.draw(ResourceManager.getImage("fullBright.png"), mainCamera.position.x -GAME_WIDTH/2 * mainCamera.zoom, mainCamera.position.y -GAME_HEIGHT/2 * mainCamera.zoom, GAME_WIDTH * mainCamera.zoom, GAME_HEIGHT * mainCamera.zoom);

		Collections.sort(drawLights, drawLightComp);
		for (DrawLight g : drawLights) {
			spriteBatch.setColor(new Color(1.0f, 1f, 1f, 1));
			g.renderLight(spriteBatch);
		}

		spriteBatch.setBlendFunction(Gdx.gl.GL_ZERO, Gdx.gl.GL_SRC_COLOR);
		spriteBatch.draw(fb.getColorBufferTexture(), mainCamera.position.x - GAME_WIDTH/2 * mainCamera.zoom, mainCamera.position.y - GAME_HEIGHT/2 * mainCamera.zoom, GAME_WIDTH * mainCamera.zoom, GAME_HEIGHT * mainCamera.zoom, 0, 0, fb.getWidth(), fb.getHeight(), false, true);

		spriteBatch.end();

		shapeBatch.setColor(Color.WHITE);
		shapeBatch.begin(ShapeRenderer.ShapeType.Line);
		for (Float[] line : debugDrawLines) {
			shapeBatch.point(line[0], line[1], 1);
			shapeBatch.point(line[2], line[3], 1);
			shapeBatch.line(line[0], line[1], line[2], line[3]);
		}
		shapeBatch.end();

		spriteBatch.setBlendFunction(Gdx.gl.GL_SRC_ALPHA, Gdx.gl.GL_ONE_MINUS_SRC_ALPHA);
		Vector3 tmpPos = new Vector3(mainCamera.position);
		float tmpZoom = mainCamera.zoom;

		mainCamera.position.set(0, 0, 0);
		mainCamera.zoom = 5;
		mainCamera.update();
		spriteBatch.setProjectionMatrix(mainCamera.combined);
		spriteBatch.begin();
		Collections.sort(drawObjectsGUI, drawGUIComp);
		for (DrawableGUI g : drawObjectsGUI) {
			g.renderGUI(spriteBatch);
		}
		if (profile) {
			Iterator<String> keys = profiles.keySet().iterator();
			int i = 0;
			while (keys.hasNext()) {
				String key = keys.next();
				//System.out.println(profiles.get(key).toString());
				spriteBatch.setColor(Color.WHITE);
				StringDraw sd = new StringDraw(profiles.get(key).toString(), -GAME_WIDTH / 2 * 5, +GAME_HEIGHT / 2 * 5 - 32 * i, 0, 0);
				sd.update();
				sd.renderGUI(spriteBatch);
				i += 1;
			}
			currentProfile += Time.deltaTime;
			if (currentProfile >= profileInterval) {
				profiles.clear();
				currentProfile = 0;
			}
		}

		spriteBatch.end();

		mainCamera.position.set(tmpPos);
		mainCamera.zoom = tmpZoom;
		mainCamera.update();
	}

	@Override
	public void dispose () {
		spriteBatch.dispose();
		renderBatch.dispose();
		shapeBatch.dispose();
		System.exit(1);
	}

	private class classProfile {
		int count;
		String name;
		long totalTime;

		@Override
		public String toString() {
			return name + " (" + count + ") : Total Time : " + totalTime / 1000000f + "ms , Average Time : " + totalTime/count / 1000000f + " ms";
		}
	}
}
